<?php
  session_start();
  include 'koneksi.php';
  if(isset($_SESSION['admin'])){
    header('location:admin/index.php');
  }if(isset($_SESSION['pelanggan'])){
    header('location:index.php');
  }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/home11-grid.html   11 Nov 2019 12:31:50 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Login</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="template-index home11-grid">
<div id="pre-loader">
    <img src="assets/images/loader.gif" alt="Loading..." />
</div>
<div class="pageWrapper">
	<!--Search Form Drawer-->
    <!--End Search Form Drawer-->
     <!-- header -->
     <?php include "header.php" ?>
    <!-- header -->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Login</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 main-col offset-md-3">
                <?php 
                      if(isset($_POST['login'])){
                        $getDataAdmin    = $koneksi->query("SELECT * FROM admin WHERE username = '$_POST[username]' AND password = '$_POST[pass]'");
                        $checkDataAdmin  = $getDataAdmin->num_rows;
                        $getDataPelanggan    = $koneksi->query("SELECT * FROM Pelanggan WHERE email_pelanggan = '$_POST[username]' AND password_pelanggan = '$_POST[pass]'");
                        $checkDataPelanggan  = $getDataPelanggan->num_rows;
                        if($checkDataAdmin > 0){
                          $_SESSION['admin'] = $getDataAdmin->fetch_assoc();
                          echo "<div class='alert alert-info'>Login Sukses</div>";
                          echo "<meta http-equiv='refresh' content=1;url='admin/index.php'>";
                        }else  if($checkDataPelanggan > 0){
                          $_SESSION['pelanggan'] = $getDataPelanggan->fetch_assoc();
                          echo "<div class='alert alert-info'>Login Sukses</div>";
                          echo "<meta http-equiv='refresh' content=1;url='index.php'>";
                        }else{
                          echo "<div class='alert alert-danger'>Login Gagal</div>";
                          // echo "<meta http-equiv='refresh' content=1;url='login.php'>";
                        } 
                      }
                     ?>
                	<div class="mb-4">
                       <form method="post" action="#" id="CustomerLoginForm" accept-charset="UTF-8" class="contact-form">	
                          <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerEmail">Username</label>
                                    <input type="text" name="username" placeholder="" id="CustomerEmail" class="" autocorrect="off" autocapitalize="off" autofocus="">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerPassword">Password</label>
                                    <input type="password" value="" name="pass" placeholder="" id="CustomerPassword" class="">                        	
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="text-center col-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn mb-3" name="login" value="Sign In">
                                <p class="mb-4">
									<a href="#" id="RecoverPassword">Forgot your password?</a> &nbsp; | &nbsp;
								    <a href="register.php" id="customer_register_link">Create account</a>
                                </p>
                            </div>
                         </div>
                     </form>
                     
                    </div>
               	</div>
            </div>
        </div>
        
    </div>
    <!-- end content -->
    <!-- footer -->
    <?php include "footer.php" ?>
    <!-- end footer -->
    <span id="site-scroll" style="display: inline;"><i class="icon anm anm-angle-up-r"></i></span>
    
   
    
   
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     <!--For Newsletter Popup-->
     
    <!--End For Newsletter Popup-->
</div>
</body>

<!-- belle/home11-grid.html   11 Nov 2019 12:32:59 GMT -->
</html>