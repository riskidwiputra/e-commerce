<?php
  session_start();
  include 'koneksi.php';
  if(isset($_SESSION['session_admin'])){
    header('location:admin/index.php');
  }if(isset($_SESSION['session_pelanggan'])){
    header('location:index.php');
  }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/about-us.html   11 Nov 2019 12:44:33 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>About Us &ndash; Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle">
<div class="pageWrapper">
	<!--Search Form Drawer-->
	
    <!--End Search Form Drawer-->
   <?php include "header.php" ?>
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">About Us</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<div class="text-center mb-4">
                        <h2 class="h2">Achats Indonesia</h2>
                        <div class="rte-setting">
                            <p><strong>Belanja Online Fashion & Lifestyle Terbaru.</strong></p>
                            <p>Temukan nuansa baru dari dunia fashion dengan produk yang terbaru dan berkualitas untuk melengkapi kebutuhan fashion Anda. Jika nanti pesanan tidak memuaskan Anda, nikmati gratis 30 hari pengembalian dengan mudah di seluruh Indonesia.</p>
                        </div>
                    </div>
               	</div>
            </div>
            <div class="row">
            	<div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-4"><img class="blur-up lazyload" data-src="assets/images/about1.jpg" src="assets/images/about1.jpg" alt="About Us" /></div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-4"><img class="blur-up lazyload" data-src="assets/images/about2.jpg" src="assets/images/about2.jpg" alt="About Us" /></div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-4"><img class="blur-up lazyload" data-src="assets/images/about3.jpg" src="assets/images/about3.jpg" alt="About Us" /></div>
            </div>
            
            <div class="row">
            	<div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-4">
                	<h2 class="h2">About Achats Indonesia</h2>
                    <div class="rte-setting"><p><strong>Achats Indonesia didirikan pada tahun 2021. Saat ini, Achats Indonesia merupakan retail online fashion dengan perkembangan paling pesat di Asia. Situs di setiap negara memastikan bahwa produk fashion disesuaikan dengan selera negara masing-masing dan mengadaptasi preferensinya.</strong></p>
                    <p> Kami menawarkan pakaian wanita, pakaian pria, sepatu, aksesoris, perlengkapan olahraga, dan banyak lagi! Hal inilah yang membuat kami menjadi tujuan utama fashion online di Indonesia. Hanya dalam beberapa tahun, kami telah merevolusi skena mode di Asia, dimulai dari kebiasaan berbelanja Anda hingga membentuk gaya personal Anda.</p>
                    <p></p>
                    <p>Kami memberikan Anda banyak sekali pilihan untuk tetap menjadi seseorang yang stylish. Kami adalah sarana penyedia fashion yang diimbangi dengan teknologi terkini yang akan memberikan Anda pengalaman berbelanja online yang tak tertandingin.</p></div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                	<h2 class="h2">Contact Us</h2>
                    <ul class="addressFooter">
                        <li><i class="icon anm anm-map-marker-al"></i><p>55 Gallaxy Enque, Jl. Karya Jaya No. 12 A, 20144 MDN</p></li>
                        <li class="phone"><i class="icon anm anm-phone-s"></i><p>(+62) 822 7340 1845</p></li>
                        <li class="email"><i class="icon anm anm-envelope-l"></i><p>achats@gmail.com</p></li>
                    </ul>
                    <hr />
                </div>
            </div>
            
            
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php include "footer.php" ?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>

<!-- belle/about-us.html   11 Nov 2019 12:44:39 GMT -->
</html>