<?php
  session_start();
  include 'koneksi.php';
?>

<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/home11-grid.html   11 Nov 2019 12:31:50 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="template-index home2-default">
<div id="pre-loader">
    <img src="assets/images/loader.gif" alt="Loading..." />
</div>
<div class="pageWrapper">
	  <!--Search Form Drawer-->
	  
    <!--End Search Form Drawer-->

    <!-- header -->
    <?php include "header.php" ?>
    <!-- header -->

    <!--Body Content-->
    <div id="page-content">
      
    	<!--Home slider-->
    	<div class="slideshow slideshow-wrapper pb-section">
        <div class="home-slideshow">
          <div class="slide">
              <div class="blur-up lazyload">
                <img class="blur-up lazyload" data-src="assets/images/slideshow-banners/home11-grid-banner1.jpg" src="assets/images/slideshow-banners/home11-grid-banner1.jpg" alt="Summer Hot Collection" title="Summer Hot Collection" />
                <div class="slideshow__text-wrap slideshow__overlay classic middle">
                  <div class="slideshow__text-content middle">
                    <div class="wrap-caption right">
                      <h2 class="h1 mega-title slideshow__title">Ayo Segera Belanja!</h2>
                      <span class="mega-subtitle slideshow__subtitle">Banyak pilihan disini.</span>
                      <span class="btn"><a href="shop.php" style="color: white" >Shop now</a></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="slide">
                	<div class="blur-up lazyload">
                        <img class="blur-up lazyload" data-src="assets/images/slideshow-banners/home11-grid-banner2.jpg" src="assets/images/slideshow-banners/home11-grid-banner2.jpg" alt="Summer Bikini Collection" title="Summer Bikini Collection" />
                        <div class="slideshow__text-wrap slideshow__overlay classic middle">
                            <div class="slideshow__text-content middle">
                                <div class="wrap-caption left">
                                    <h2 class="h1 mega-title slideshow__title">Cari Stylemu Disini!</h2>
                                    <span class="mega-subtitle slideshow__subtitle">Segera tentukan lifestylemu.</span>
                                    <span class="btn btn-primary"><a href="shop.php" style="color: white">Shop now</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
      </div>
      <!-- slide -->
      <div class="tab-slider-product section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="section-header text-center">
                        <h2 class="h2 heading-font">Top Sellers</h2>
                    </div>
                    <div class="tabs-listing">
                        <ul class="tabs clearfix">
                          <?php $i=1; $getkategori = $koneksi->query("SELECT * FROM kategori");
                                    while($rowkategori = $getkategori->fetch_assoc()){
                          ?>
                            <li <?php if($i == 1){ ?>class="active" <?php } ?> rel="tab<?= $i ?>"><?= $rowkategori['nama_kategori'] ?></li>
                            <!-- <li rel="tab2">Men</li>
                            <li rel="tab3">Sale</li> -->
                          <?php $i++; } ?>
                        </ul>
                        
                        <div class="tab_container">
                          <?php $i=1; $getkategori = $koneksi->query("SELECT * FROM kategori");
                                    while($rowkategori = $getkategori->fetch_assoc()){
                                     
                          ?>
                          
                            <div id="tab<?= $i ?>" class="tab_content grid-products style3">
                                <div class="productSlider">
                                  
                                    
                                    <?php 
                                    $id_kategori  = $rowkategori['id_kategori'];
                                  
                                    $getProduk    = $koneksi->query("SELECT * FROM produk WHERE id_kategori = '$id_kategori' ORDER BY terjual DESC LIMIT 6 ");
                                    if($getProduk->num_rows > 0){
                                    while($dataProduk = $getProduk->fetch_assoc()){ 
                                    ?>
                                      <div class="col-12 item">
                                      <!-- product name -->
                                        <div class="product-name">
                                            <a href="product-layout-1.html"><?= $dataProduk['nama_produk'] ?></a>
                                        </div>
                                        <!-- End product name -->
                                        <!-- start product image -->
                                        <div class="product-image">
                                            <!-- start product image -->
                                            <a href="detail.php?id=<?= $dataProduk['id_produk'] ?>">
                                                <!-- image -->
                                                <img class="blur-up lazyload" data-src="admin/images/<?= $dataProduk['foto'] ?>" src="images/<?= $dataProduk['foto'] ?>" alt="image" title="product" style="height:300px">
                                               
                                                <!-- End image -->
                                                <!-- Hover image -->
                                                <!-- <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image2-1.jpg" src="assets/images/product-images/product-image2-1.jpg" alt="image" title="product"> -->
                                                <!-- End hover image -->
                                            </a>
                                            <!-- end product image -->
                                        </div>
                                        <!-- end product image -->
                                        
                                        <div class="button-set button-style2">
                                            <form class="variants add btn-style2" action="beli.php?id=<?= $dataProduk['id_produk'] ?>" onclick="window.location.href='beli.php?id=<?= $dataProduk['id_produk'] ?>'" method="post">
                                                <button class="btn cartIcon btn-addto-cart" type="button" tabindex="0"><i class="icon anm anm-bag-l"></i></button>
                                            </form>
                                           
                                            <a href="javascript:void(0)" title="Quick View" class="btn-style2 quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview" data-nama_produk="<?= $dataProduk['nama_produk'] ?>" data-harga_produk="<?= $dataProduk['harga_produk'] ?>" data-foto="admin/images/<?= $dataProduk['foto'] ?> " data-deskripsi="<?= $dataProduk['deskripsi'] ?>" data-size="<?= $dataProduk['size'] ?>" data-warna="<?= $dataProduk['warna'] ?>">
                                                <i class="icon anm anm-search-plus-r"></i>
                                            </a>
                                            
                                           
                                        </div>
    
                                        <!--start product details -->
                                        <div class="product-details text-center">
                                            <!-- product price -->
                                            <div class="product-price">
                                                <span class="price"><?=	"Rp. ".number_format($dataProduk['harga_produk'],0,',','.').",-"; ?></span>
                                            </div>
                                            <!-- End product price -->
                                           
                                        </div>
                                        <!-- End product details -->
                                    </div>
                                    <?php }} ?>
                                </div>
                            </div>
                            
                          <?php $i++; } ?>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
      </div>
      <div class="section">
            <div class="hero hero--large hero__overlay bg-size" style="background-image: url(&quot;assets/images/parallax-banners/parallax-banner.jpg&quot;); background-size: cover; background-position: center center; background-repeat: no-repeat;">
            	<img class="bg-img" src="assets/images/parallax-banners/parallax-banner.jpg" alt="" style="display: none;">
                <div class="hero__inner">
                    <div class="container">
                        <div class="wrap-text left text-small font-bold">
                            <h2 class="h2 mega-title">Achats <br> The best choice for your store</h2>
                            <div class="rte-setting mega-subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                            <a href="shop.php" class="btn">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      <!--End Body Content-->
      <div class="section imgBanners pb-0">
        <div class="imgBnrOuter">
          <div class="container">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-header text-center">
                  <h2 class="h2">New Product</h2>
                  <p>Determine the style according to your taste<br>And always have a piece of art that you made somewhere in the outside.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="container-fluid">
            <div class="row">
            <!-- looping -->
          
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="grid-products">
                  <div class="row">
                  <?php $GetData = $koneksi->query("SELECT * FROM produk ORDER BY created_at DESC LIMIT 4");  
                      while($rows = $GetData->fetch_array()){
                  ?>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 item">
                      <!-- start product image -->
                      <div class="product-image">
                          <!-- start product image -->
                        <a href="detail.php?id=<?= $rows['id_produk'] ?>" class="grid-view-item__link">
                          <!-- image -->
                          <div class="inner topright">
                              <img data-src="admin/images/<?= $rows['foto'] ?>" src="admin/images/<?= $rows['foto'] ?>" alt="Halloween Look" title="Halloween Look" width="270px" height="270px" class=" blur-up lazyloaded">
                          </div>
                        </a>
                        <!-- end product image -->

                        <!-- Start product button -->
                        <form class="variants add" action="beli.php?id=<?= $rows['id_produk'] ?>" onclick="window.location.href='beli.php?id=<?= $rows['id_produk'] ?>'" method="post">
                            <button class="btn btn-addto-cart" type="button" tabindex="0">Add To Cart</button>
                        </form>
                        <div class="button-set">
                          <a href="javascript:void(0)" title="Quick View" class="quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview" data-nama_produk="<?= $rows['nama_produk'] ?>" data-harga_produk="<?= $rows['harga_produk'] ?>" data-foto="admin/images/<?= $rows['foto'] ?> " data-deskripsi="<?= $rows['deskripsi'] ?>" data-size="<?= $rows['size'] ?>" data-warna="<?= $rows['warna'] ?>">
                              <i class="icon anm anm-search-plus-r"></i>
                          </a>
                         <!--  <div class="wishlist-btn">
                              <a class="wishlist add-to-wishlist" href="wishlist.html">
                                  <i class="icon anm anm-heart-l"></i>
                              </a>
                          </div> -->
                          
                        </div>
                          <!-- end product button -->
                      </div>
                      <!-- end product image -->
                      <!--start product details -->
                      <div class="product-details text-center">
                          <!-- product name -->
                          <div class="product-name">
                              <a href="product-layout-1.html"><?= $rows['nama_produk'] ?></a>
                          </div>
                          <!-- End product name -->
                          <!-- product price -->
                          <div class="product-price">
                              <!-- <span class="old-price">$500.00</span> -->
                              <span class="price"><?=	"Rp. ".number_format($rows['harga_produk'],0,',','.').",-"; ?></span>
                          </div>
                          <!-- End product price -->
                      </div>
                        <!-- End product details -->
                    </div> 
                    <?php } ?>     
                  </div>            
                </div>
              </div>
          
              <!-- looping -->
            </div>    
          </div>
        </div>
      </div>
  
        <!--Testimonial Slider-->
        <div class="section testimonial-slider">
        	<div class="container-fluid">
                <div class="quote-wraper">
                    <!--Testimonial Slider Title-->
                    <div class="section-header text-center">
                        <h2 class="h2">What They're Saying</h2>          
                    </div>
                    <!--End Testimonial Slider Title-->
                    <!--Testimonial Slider Items-->
                    <div class="quotes-slider">
                    	<div class="quotes-slide">
                            <blockquote class="quotes-slider__text text-center">             
                              <div class="rte-setting"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                              <br><br>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div>
                              <p class="authour">Happy Customer</span></p>
                            </blockquote>
                        </div>
                        <div class="quotes-slide">
                            <blockquote class="quotes-slider__text text-center">             
                              <div class="rte-setting"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>
                              <p class="authour">Happy Customer</p>
                            </blockquote>
                        </div>
                        <div class="quotes-slide">
                            <blockquote class="quotes-slider__text text-center">             
                              <div class="rte-setting"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div>
                              <p class="authour">Happy Customer</span></p>
                            </blockquote>
                        </div>
                    </div>
                    <!--Testimonial Slider Items-->
                </div>
            </div>
        </div>
        <!--End Testimonial Slider-->

                                <!--Store Feature-->
        <div class="store-feature section">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                      <ul class="display-table store-info">
                          <li class="display-table-cell">
                              <i class="icon anm anm-repeat-alt"></i>
                              <h5>Free Shipping &amp; Return</h5>
                              <span class="sub-text">Free shipping on all US orders</span>
                            </li>
                            <li class="display-table-cell">
                              <i class="icon anm anm-money-bill-ar"></i>
                                <h5>Money Guarantee</h5>
                                <span class="sub-text">30 days money back guarantee</span>
                            </li>
                            <li class="display-table-cell">
                              <i class="icon anm anm-thumbs-up-l"></i>
                                <h5>Online Support</h5>
                                <span class="sub-text">We support online 24/7 on day</span>
                            </li>
                            <li class="display-table-cell">
                              <i class="icon anm anm-check-sql"></i>
                                <h5>Secure Payments</h5>
                                <span class="sub-text">All payment are Secured and trusted.</span>
                          </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--End Store Feature-->
      


    </div>
    
   <?php include "footer.php" ?>
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
    <!--Quick View popup-->
    <div class="modal fade quick-view-popup" id="content_quickview">
    	<div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div id="ProductSection-product-template" class="product-template__container prstyle1">
              <div class="product-single">
                <!-- Start model close -->
                <a href="javascript:void()" data-dismiss="modal" class="model-close-btn pull-right" title="close"><span class="icon icon anm anm-times-l"></span></a>
                <!-- End model close -->
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="product-details-img">
                      <div class="pl-20" id="mdl_gambar">
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="product-single__meta">
                      <h2 class="product-single__title" id="mdl_nama_produk"></h2>
                      <div class="prInfoRow">
                          <div class="product-stock"> <span class="instock ">In Stock</span> <span class="outstock hide">Unavailable</span> </div>
                      </div>
                      <p class="product-single__price product-single__price-product-template">
                          <span class="visually-hidden">Regular price</span>
                          <!-- <s id="ComparePrice-product-template"><span class="money">$600.00</span></s> -->
                          <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
                              <span id="ProductPrice-product-template"><span class="money" id="mdl_harga_produk"></span></span>
                          </span>
                      </p>
                      <div class="product-single__description rte" id="mdl_deskripsi">
                          
                      </div>
                        
                      <form method="post" action="http://annimexweb.com/cart/add" id="product_form_10508262282" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
                        <div class="swatch clearfix swatch-0 option1" data-option-index="0">
                            <div class="product-form__item" id="get_warna">
                            </div>
                        </div>
                        <div class="swatch clearfix swatch-1 option2" data-option-index="1">
                            <div class="product-form__item" id="sizedetail">
                              
                            </div>
                        </div>
                        <!-- Product Action -->
                        <div class="product-action clearfix">
                          <div class="product-form__item--quantity">
                            <div class="wrapQtyBtn">
                              <div class="qtyField">
                                  <a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
                                  <input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
                                  <a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
                              </div>
                            </div>
                          </div>                                
                          <div class="product-form__item--submit">
                              <button type="button" name="add" class="btn product-form__cart-submit">
                                  <span>Add to cart</span>
                              </button>
                          </div>
                        </div>
                        <!-- End Product Action -->
                      </form>
                      <div class="display-table shareRow">
                        <div class="display-table-cell">
                            <!-- <div class="wishlist-btn">
                                <a class="wishlist add-to-wishlist" href="#" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Add to Wishlist</span></a>
                            </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--End-product-single-->
              </div>
            </div>
        	</div>
        </div>
      </div>
    </div>
    <!-- End Newsletter Popup -->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <script src="assets/js/vendor/instagram-feed.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     <!--Instagram Js-->
     
    <!--End For Newsletter Popup-->
    <!--End For Newsletter Popup-->
</div>
</body>

<!-- belle/home11-grid.html   11 Nov 2019 12:32:59 GMT -->
</html>
<script type="text/javascript">
  $('.quick-view').on("click", function(){
    var nama_produk = $(this).data('nama_produk');
    var harga       = $(this).data('harga_produk');
    var foto        = $(this).data('foto');
    var deskripsi   = $(this).data('deskripsi');
    var size        = $(this).data('size');
    var warna       = $(this).data('warna');
    console.log(size);
    var gantirp     = formatRupiah(String(harga), 'Rp. ');
    if(warna == "hitam" ){
      var dataWarna =  ` <label class="header">Color: <span class="slVariant">`+warna+`</span></label>
                        <div data-value="Black" class="swatch-element color black available">
                            <input class="swatchInput" id="swatch-0-black" type="radio" name="option-0" value="Black"><label class="swatchLbl color small rounded" for="swatch-0-black" style="background-color:black;" title="Black"></label>
                        </div>`;
    }else if(warna == "putih"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label> <div data-value="Dark White" class="swatch-element color dark-white available">
                          <input class="swatchInput" id="swatch-0-dark-white" type="radio" name="option-0" value="Dark White"><label class="swatchLbl color small rounded" for="swatch-0-dark-white" style="background-color:white;" title="Dark white"></label>
                        </div>`;
    }else if(warna == "merah"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label> <div data-value="Dark Red" class="swatch-element color dark-red available">
                          <input class="swatchInput" id="swatch-0-dark-red" type="radio" name="option-0" value="Dark Red"><label class="swatchLbl color small rounded" for="swatch-0-dark-red" style="background-color:red;" title="Dark red"></label>
                        </div>`;
     
    }
    else if(warna == "biru"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label><div data-value="Dark Blue" class="swatch-element color dark-blue available">
                          <input class="swatchInput" id="swatch-0-dark-blue" type="radio" name="option-0" value="Dark Blue"><label class="swatchLbl color small rounded" for="swatch-0-dark-blue" style="background-color:blue;" title="Dark blue"></label>
                        </div>`;
  
      
    }
    else if(warna == "pink"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label> <div data-value="Dark Pink" class="swatch-element color dark-pink available">
                          <input class="swatchInput" id="swatch-0-dark-pink" type="radio" name="option-0" value="Dark Pink"><label class="swatchLbl color small rounded" for="swatch-0-dark-pink" style="background-color:pink;" title="Dark pink"></label>
                        </div>`;
    }
    else if(warna == "abu-abu"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label> <div data-value="Dark Gray" class="swatch-element color dark-gray available">
                          <input class="swatchInput" id="swatch-0-dark-gray" type="radio" name="option-0" value="Dark Gray"><label class="swatchLbl color small rounded" for="swatch-0-dark-gray" style="background-color:gray;" title="Dark gray"></label>
                        </div>`;
    }
    
    else if(warna == "hijau"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label><div data-value="Dark Green" class="swatch-element color dark-green available">
                          <input class="swatchInput" id="swatch-0-dark-green" type="radio" name="option-0" value="Dark Green"><label class="swatchLbl color small rounded" for="swatch-0-dark-green" style="background-color:darkgreen;" title="Dark Green"></label>
                        </div>`;
    }
    else if(warna == "kuning"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label><div data-value="Dark Yellow" class="swatch-element color dark-yellow available">
                          <input class="swatchInput" id="swatch-0-dark-yellow" type="radio" name="option-0" value="Dark Yellow"><label class="swatchLbl color small rounded" for="swatch-0-dark-yellow" style="background-color:yellow;" title="Dark yellow"></label>
                        </div>`;
    }
    else if(warna == "ungu"){
      var dataWarna =  `<label class="header">Color: <span class="slVariant">`+warna+`</span></label><div data-value="Dark purple" class="swatch-element color dark-purple available">
                          <input class="swatchInput" id="swatch-0-dark-purple" type="radio" name="option-0" value="Dark purple"><label class="swatchLbl color small rounded" for="swatch-0-dark-purple" style="background-color:purple;" title="Dark purple"></label>
                        </div>`;
    }


    if(size == "xs" ){
      var dataWarna =  ` <label class="header">Size: <span class="slVariant">`+size+`</span></label> <div data-value="XS" class="swatch-element xs available">
                          <input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
                          <label class="swatchLbl medium rectangle" for="swatch-1-xs" title="XS">XS</label>
                        </div>`;
    }else if(size == "s"){
      var datasize =  `<label class="header">Size: <span class="slVariant">`+size+`</span></label><div data-value="S" class="swatch-element s available">
                          <input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
                          <label class="swatchLbl medium rectangle" for="swatch-1-s" title="S">S</label>
                        </div>`;
    }else if(size == "m"){
      var datasize =  `<label class="header">Size: <span class="slVariant">`+size+`</span></label><div data-value="M" class="swatch-element m available">
                          <input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
                          <label class="swatchLbl medium rectangle" for="swatch-1-m" title="M">M</label>
                        </div>`;
     
    }
    else if(size == "l"){
      var datasize =  `<label class="header">Size: <span class="slVariant">`+size+`</span></label><div data-value="L" class="swatch-element l available">
                          <input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="L">
                          <label class="swatchLbl medium rectangle" for="swatch-1-l" title="L">L</label>
                        </div>`;
  
      
    }
    else if(size == "xl"){
      var datasize =  `<label class="header">Size: <span class="slVariant">`+size+`</span></label><div data-value="XL" class="swatch-element xl available">
                          <input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="XL">
                          <label class="swatchLbl medium rectangle" for="swatch-1-l" title="Xl">XL</label>
                        </div>`;
    }
    else if(size == "abu-abu"){
      var datasize =  `<label class="header">Color: <span class="slVariant">`+datasize+`</span></label> <div data-value="Dark Gray" class="swatch-element color dark-gray available">
                          <input class="swatchInput" id="swatch-0-dark-gray" type="radio" name="option-0" value="Dark Gray"><label class="swatchLbl color small rounded" for="swatch-0-dark-gray" style="background-color:gray;" title="Dark gray"></label>
                        </div>`;
    }
    
    else if(size == "hijau"){
      var datasize =  `<label class="header">Color: <span class="slVariant">`+size+`</span></label><div data-value="Dark Green" class="swatch-element color dark-green available">
                          <input class="swatchInput" id="swatch-0-dark-green" type="radio" name="option-0" value="Dark Green"><label class="swatchLbl color small rounded" for="swatch-0-dark-green" style="background-color:darkgreen;" title="Dark Green"></label>
                        </div>`;
    }
    else if(size == "kuning"){
      var datasize =  `<label class="header">Color: <span class="slVariant">`+size+`</span></label><div data-value="Dark Yellow" class="swatch-element color dark-yellow available">
                          <input class="swatchInput" id="swatch-0-dark-yellow" type="radio" name="option-0" value="Dark Yellow"><label class="swatchLbl color small rounded" for="swatch-0-dark-yellow" style="background-color:yellow;" title="Dark yellow"></label>
                        </div>`;
    }
    else if(size == "ungu"){
      var datasize =  `<label class="header">Color: <span class="slVariant">`+size+`</span></label><div data-value="Dark purple" class="swatch-element color dark-purple available">
                          <input class="swatchInput" id="swatch-0-dark-purple" type="radio" name="option-0" value="Dark purple"><label class="swatchLbl color small rounded" for="swatch-0-dark-purple" style="background-color:purple;" title="Dark purple"></label>
                        </div>`;
    }

    var gambar = `<img src="`+foto+`" alt="" style="width:100%;height:100%"/>`;
                                      
                                      
                                      
                                      

    $("#mdl_nama_produk").html(nama_produk);
    $("#mdl_harga_produk").html(gantirp);
    $("#mdl_deskripsi").html(deskripsi);
    $("#get_warna").html(dataWarna);
    $("#sizedetail").html(datasize);
    $("#mdl_gambar").html(gambar);

       
    
  })
  function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
