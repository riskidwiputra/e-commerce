    <div class="search">
        <div class="search__form">
            <form class="search-bar__form" action="shop.php" method="get">
                <button class="go-btn search__button" type="submit"><i class="icon anm anm-search-l"></i></button>
                <input class="search__input" type="search" name="search" value="" placeholder="Search entire store..." aria-label="Search" autocomplete="off">
            </form>
            <button type="button" class="search-trigger close-btn"><i class="anm anm-times-l"></i></button>
        </div>
    </div>
    <!--Top Header-->
    <div class="top-header">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-10 col-sm-8 col-md-5 col-lg-4">
                    
                  <p class="phone-no"><i class="anm anm-phone-s"></i> +62 822 7340 1845</p>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 d-none d-lg-none d-md-block d-lg-block">
                  <div class="text-center"><p class="top-header_middle-text"> Indonesia Express Shipping</p></div>
                </div>
                <div class="col-2 col-sm-4 col-md-3 col-lg-4 text-right">
                	<span class="user-menu d-block d-lg-none"><i class="anm anm-user-al" aria-hidden="true"></i></span>
                    <ul class="customer-links list-inline">
                        <?php if(!isset($_SESSION['pelanggan'])){ ?>
                          <li><a href="login.php">Login</a></li>
                          <li><a href="register.php">Create Account</a></li>
                        <?php }else{ ?>
                          <li><a href="logout.php">Logout</a></li>
                          <li><a href="pesan.php">Pesanan Saya</a></li>
                        <?php } ?>
                        
                        <li
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--End Top Header-->
    <!--Header-->
    <div class="header-wrap animated d-flex border-bottom">
    	<div class="container-fluid">        
            <div class="row align-items-center">
            	<!--Desktop Logo-->
                <div class="logo col-md-2 col-lg-2 d-none d-lg-block">
                    <a href="index.php">
                    	<img src="assets/images/logo-achats-terbaru.png" alt="Belle Multipurpose Html Template" title="Belle Multipurpose Html Template" />
                    </a>
                </div>
                <!--End Desktop Logo-->
                <div class="col-2 col-sm-3 col-md-3 col-lg-8">
                	<div class="d-block d-lg-none">
                        <button type="button" class="btn--link site-header__menu js-mobile-nav-toggle mobile-nav--open">
                        	<i class="icon anm anm-times-l"></i>
                            <i class="anm anm-bars-r"></i>
                        </button>
                    </div>
                	<!--Desktop Menu-->
                	<nav class="grid__item" id="AccessibleNav"><!-- for mobile -->
                    <ul id="siteNav" class="site-nav medium center hidearrow">
                        <li class="lvl1 parent megamenu"><a href="index.php">Home <i class="anm anm-angle-down-l"></i></a>
                        </li>
                        <li class="lvl1 parent dropdown"><a href="#">Shop <i class="anm anm-angle-down-l"></i></a>
                          <ul class="dropdown">
                            <?php $getkategori = $koneksi->query("SELECT * FROM kategori");
                                  while($rowkategori = $getkategori->fetch_assoc()){
                            ?>
                            <li><a href="#" class="site-nav"><?= $rowkategori['nama_kategori'] ?><i class="anm anm-angle-right-l"></i></a>
                              <?php 
                                    $id_kategori = $rowkategori['id_kategori'];
                                    $getsub = $koneksi->query("SELECT * FROM sub_kategori WHERE id_kategori='$id_kategori'"); 
                                    if($getsub->num_rows > 0){   
                              ?>
                                    <ul class="dropdown">
                              <?php 
                                 while($rowsub = $getsub->fetch_assoc()){
                              ?>
                                        <li><a href="shop.php?kategori=<?= $rowsub['id_sub_kategori'] ?>" class="site-nav"><?= $rowsub['nama'] ?></a></li>
                              <?php } ?>
                                    </ul>
                              <?php
                                    }
                              ?>
                            </li>
                            <?php
                              }       
                            ?>
                            </ul>
                          </li>
                        <li class="lvl1"><a href="about-us.php"><b>About Us</b></a></li>
                    
                    </ul>
                  </nav>
                    <!--End Desktop Menu-->
                </div>
                <!--Mobile Logo-->
                <div class="col-6 col-sm-6 col-md-6 col-lg-2 d-block d-lg-none mobile-logo">
                	<div class="logo">
                        <a href="index.php">
                            <img src="assets/images/logo.svg" alt="Belle Multipurpose Html Template" title="Belle Multipurpose Html Template" />
                        </a>
                    </div>
                </div>
                <!--Mobile Logo-->
                <div class="col-4 col-sm-3 col-md-3 col-lg-2">
                	<div class="site-cart">
                    	
                     
                            <a href="keranjang.php" style="font-size: 21px;" >
                              <i class="icon anm anm-bag-l"></i>
                              <?php if(isset($_SESSION['keranjang'])){ ?>
                              <span id="" class="site-header__cart-count"><?= count($_SESSION['keranjang']); ?></span>
                                <?php }else{ ?>
                                   <span id="" class="site-header__cart-count">0</span>
                                <?php } ?>
                            </a>
                        
                          
                         
                         
                      
                  </div>
                  <div class="site-header__search">
                    	<button type="button" class="search-trigger"><i class="icon anm anm-search-l"></i></button>
                  </div>
                </div>
        	</div>
        </div>
    </div>
    <!--End Header-->
    <!--Mobile Menu-->
    <div class="mobile-nav-wrapper" role="navigation">
		<div class="closemobileMenu"><i class="icon anm anm-times-l pull-right"></i> Close Menu</div>
        <ul id="MobileNav" class="mobile-nav">
        	<li class="lvl1 parent megamenu"><a href="index.html">Home </a>
        </li>
        <li class="lvl1 parent dropdown"><a href="#">Shop <i class="anm anm-angle-down-l"></i></a>
          <ul class="dropdown">
          <?php $getkategori = $koneksi->query("SELECT * FROM kategori");
                    while($rowkategori = $getkategori->fetch_assoc()){
            ?>
            <li><a href="#" class="site-nav"><?= $rowkategori['nama_kategori'] ?><i class="anm anm-angle-right-l"></i></a>
                <?php 
                    $id_kategori = $rowkategori['id_kategori'];
                    $getsub = $koneksi->query("SELECT * FROM sub_kategori WHERE id_kategori='$id_kategori'"); 
                    if($getsub->num_rows > 0){   
                ?>
                    <ul class="dropdown">
                <?php 
                    while($rowsub = $getsub->fetch_assoc()){
                ?>
                        <li><a href="#" class="site-nav"><?= $rowsub['nama'] ?></a></li>
                <?php } ?>
                    </ul>
                <?php
                    }
                ?>
            </li>
            <?php
                }       
            ?>
          </ul>
        </li>	
        <li class="lvl1"><a href="#"><b>About Us</b></a>
        </li>
      </ul>
	</div>
	<!--End Mobile Menu-->