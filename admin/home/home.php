 <div class="container-fluid">
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- Content Menampilkan Tagihan -->
    <div class="col-lg-12 col-xlg-9 col-md-7 " >
        <div class="card" >
             <div class="table-responsive" >
               <div class="card-block " >
                <!-- Untuk !! -->
                <div class="card float-left " style="width: 300px; margin-right: 20px; margin-left: 15px;">
                	<div class="card-body bg-success" style="padding: 20px;">
                		<span class="mdi mdi-account-multiple" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center  float-right mr-2" style="font-size: 24px;">
						<?php $getadmin = $koneksi->query("SELECT * FROM admin");
							echo $getadmin->num_rows;
						?>
                		<br>
                			Admin	
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=akun" class="text-light">Detail</a>
                	</div>

                </div>
                <!-- Contoh -->

                <!-- Untuk !! -->
                <div class="card float-left" style="width: 300px; margin-right: 20px; ">
                	<div class="card-body bg-primary" style="padding: 20px;">
                		<span class="mdi mdi-account-multiple" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center float-right mr-2" style="font-size: 24px;">
						<?php $kategori = $koneksi->query("SELECT * FROM kategori");
							echo $kategori->num_rows;
						?>
						<br>
                			Kategori
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=kategori" class="text-light">Detail</a>
                	</div>

                </div>
                <!-- Contoh -->
                <!-- Untuk !! -->
                <div class="card float-left" style="width: 300px;">
                	<div class="card-body bg-danger" style="padding: 20px;">
                		<span class="mdi mdi-bank" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center float-right mr-2" style="font-size: 24px;">
						<?php $pelanggan = $koneksi->query("SELECT * FROM pelanggan");
							echo $pelanggan->num_rows;
						?>
						<br>
                            Pelanggan		
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=customer" class="text-light">Detail</a>
                	</div>

                </div>
                <!-- Contoh -->
                <!-- Untuk !! -->
                <div class="card float-left" style="width: 300px; margin-right: 20px; margin-left: 15px;">
                	<div class="card-body bg-warning" style="padding: 20px;">
                		<span class="mdi mdi-account" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center float-right mr-2" style="font-size: 24px;">
						<?php $produk = $koneksi->query("SELECT * FROM produk");
							echo $produk->num_rows;
						?>
						<br>
                            Produk	
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=produk" class="text-light">Detail</a>
                	</div>

                </div>
                <!-- Contoh -->

                <!-- Untuk !! -->
                <div class="card float-left" style="width: 300px; margin-right: 20px; ">
                	<div class="card-body bg-info" style="padding: 20px;">
                		<span class="mdi mdi-file-multiple" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center float-right mr-2" style="font-size: 24px;">
						<?php $pembelian = $koneksi->query("SELECT * FROM pembelian");
							echo $pembelian->num_rows;
						?>
						<br>
                            Pembelian	
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=pembayaran" class="text-light">Detail</a>
                	</div>

                </div>
                <!-- Contoh -->
                <!-- Untuk !! -->
                <div class="card float-left" style="width: 300px;">
                	<div class="card-body bg-purple" style="padding: 20px;">
                		<span class="mdi mdi-cash-usd" style="font-size: 50px;color: white;"></span> 
                		<div class="font-weight-bold text-light text-center float-right mr-2" style="font-size: 24px;">
                			<?php $pembelian_produk = $koneksi->query("SELECT * FROM pembelian_produk");
							echo $pembelian_produk->num_rows;
						?>
						<br>
                            
                			Transaksi	
                		</div>
                	</div>
                	<div class="card-footer ">
                		<a href="?page=transaksi" class="text-light">Detail</a>

                	</div>

                </div>
                <!-- Contoh -->

            </div>
        </div>
    </div>
</div>
		
<!-- Penutup -->
               </div>