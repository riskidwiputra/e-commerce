<?php
    session_start();
    $koneksi = new mysqli("localhost","root","","ecommerce");
    if(!isset($_SESSION['admin']))
    {
        echo "<script>alert('Anda Harus Login')</script>";
        echo "<script>location='../login.php'</script>";
        header('location:../login.php');
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
   
    <title>Stores</title>
    <!-- JQUERY -->
    <link rel="stylesheet" href="data/assets/font/css/fontawesome.min.css"/>
  
    <script src="data/assets/jquery-1.10.2.js"></script>
    
    <link href="data/assets/plugins/jquery/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="data/assets/plugins/jquery/sweetalert.min.js"></script>                
    <script src="data/assets/plugins/jquery/sweetalert-dev.js"></script> 
    <!-- Bootstrap Core CSS -->
   
    <link href="data/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="data/assets/dataTables/datatables.min.css" rel="stylesheet">
    
    <!-- chartist CSS -->
    
    <!--This page css - Morris CSS -->
    <link href="data/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="data/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="data/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ========================== -->
      
       
      <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <header class="topbar">
                <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="?page=home">
                          <img src="" alt="" width="220px" height="65px">
                        </a>
                           </div>
                           <!-- ============================================================== -->
                           <!-- End Logo -->
                           <!-- ============================================================== -->
                           <div class="navbar-collapse">
                            <!-- ============================================================== -->
                            <!-- toggle and nav items -->
                            <!-- ============================================================== -->
                            <ul class="navbar-nav mr-auto mt-md-0">
                                <!-- This is  -->
                                 <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                                <!-- ============================================================== -->
                                <!-- Search -->
                                <!-- ============================================================== -->
                                
                                
                            </li>
                        </ul>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <ul class="navbar-nav my-lg-0">
                            <!-- ============================================================== -->
                            <!-- Profile -->
                            <!-- ============================================================== -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="data/assets/images/users/2.jpg" alt="user" class="profile-pic m-r-10" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                            <li> <a class="waves-effect waves-dark" href="?page=home" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Home</span></a>
                            </li>
                            <li> <a class="waves-effect waves-dark" href="?page=akun" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Akun</span></a>
                            </li>
                            <li> <a class="waves-effect waves-dark" href="?page=kategori" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">Kategori</span></a>
                            </li>
                            
                            <li> <a class="waves-effect waves-dark" href="?page=pelanggan" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Pelanggan</span></a>
                            </li>
                            <li> <a class="waves-effect waves-dark" href="?page=produk" aria-expanded="false"><i class="mdi mdi-file-multiple"></i><span class="hide-menu">Produk</span></a><li>
                            <li> <a class="waves-effect waves-dark" href="?page=pembelian" aria-expanded="false"><i class="mdi mdi-cash-usd"></i><span class="hide-menu">Pembelian</span></a>
                            </li>
                            <li> <a class="waves-effect waves-dark" href="?page=transaksi" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Transaksi</span></a>
                            </li>


                            
                        </ul>
                        
                    </nav>
                    <!-- End Sidebar navigation -->
                </div>
                <!-- End Sidebar scroll-->
                <!-- Bottom points-->
                <div class="sidebar-footer">
                    
                    <!-- item--><a href="?page=logout" class="link" data-toggle="tooltip" title="Logout"  style="width: 100%;"><i class="mdi mdi-power" ></i> Logout </a> </div>
                    <!-- End Bottom points-->
                </aside>
                
                <div class="page-wrapper">
                   
                    <?php

                if(@$_GET['page'] == 'home' || @$_GET['page'] == ''){
                      include"home/home.php";
                }else if (@$_GET['page'] == 'akun'){
                    include"akun/akun.php";
                } else if (@$_GET['page'] == 'tambah_akun'){
                    include"akun/tambah_akun.php";
                }else if (@$_GET['page'] == 'hapus_akun'){
                    include"akun/hapus_akun.php";
                }else if (@$_GET['page'] == 'edit_akun'){
                    include"akun/edit_akun.php";
                }else if (@$_GET['page'] == 'kategori'){
                    include"kategori/kategori.php";
                }else if (@$_GET['page'] == 'sub-kategori'){
                    include"sub_kategori/sub_kategori.php";
                }else if (@$_GET['page'] == 'pelanggan'){
                    include"pelanggan/pelanggan.php";

                }else if (@$_GET['page'] == 'tambah_pelanggan'){
                    include"pelanggan/tambah_pelanggan.php";
                }else if (@$_GET['page'] == 'edit_pelanggan'){
                    include"pelanggan/edit_pelanggan.php";
                }else if (@$_GET['page'] == 'hapus_pelanggan'){
                    include"pelanggan/hapus_pelanggan.php";
                }else if (@$_GET['page'] == 'produk'){
                    include"produk/produk.php";
                } else if (@$_GET['page'] == 'tambah_produk'){
                    include"produk/tambah_produk.php";
                }else if (@$_GET['page'] == 'hapus_produk'){
                    include"produk/hapus_produk.php";
                }else if (@$_GET['page'] == 'edit_produk'){
                    include"produk/edit_produk.php";
                }else if (@$_GET['page'] == 'pembelian'){
                    include"pembelian/pembelian.php";
                }else if (@$_GET['page'] == 'transaksi'){
                    include"transaksi/transaksi.php";
                }else if (@$_GET['page'] == 'logout'){
                    include"logout.php";
                }
                  ?>
              </div>
              
              <!-- footer -->
              <!-- ============================================================== -->
              <footer class="footer"> © 2021 Achats Indonesia  </footer>
              <!-- ============================================================== -->
              <!-- End footer -->
              <!-- ============================================================== -->
          </div>
          <!-- ============================================================== -->
          <!-- End Page wrapper  -->
          <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Wrapper -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- All Jquery -->
      <!-- ============================================================== -->
      <!-- Bootstrap tether Core JavaScript -->
      <script src="data/assets/plugins/bootstrap/js/tether.min.js"></script>
      <script src="data/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
      
      <script src="data/assets/dataTables/datatables.min.js"></script>

      <script type="text/javascript">
        $(document).ready( function () {
            $('#datatables').DataTable();
            $('#datatabless').DataTable();
        } );

    </script>
    
    <script>
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: "Are you sure?",
                        text: 'Hapus Data?',
                        type: "warning",
                        html: true,
                        confirmButtonColor: '#d9534f',

                        confirmButtonColor: "#DD6B55",
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>
    <script>
        jQuery(document).ready(function($){
            $('.delete-link2').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: "Kirim?",
                        text: 'Ubah Menjadi Dikirim',
                        type: "warning",
                        html: true,
                        confirmButtonColor: '#d9534f',

                        confirmButtonColor: "#DD6B55",
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>

<!-- slimscrollbar scrollbar JavaScript -->

<!--Wave Effects -->
<script src="data/js/waves.js"></script>
<!--Menu sidebar -->
<script src="data/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="data/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- chartist chart -->

<!--c3 JavaScript -->
<script src="data/assets/plugins/d3/d3.min.js"></script>
<script src="data/assets/plugins/c3-master/c3.min.js"></script>

<!-- Chart JS -->

</body>

</html>
