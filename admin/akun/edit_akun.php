<!-- Dashboard -->
<?php 
    $id_admin = $_GET['id'];
    $ambil = $koneksi->query("SELECT * FROM admin WHERE id_admin = '$id_admin'");
    $pecah = $ambil->fetch_assoc();
?>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit Akun</li>
            </ol>
        </div>
    </div>
    <!-- content -->
    <div class="card">
        <div class="card-header">
            <b><font style="font-weight: bold; font-size: 20px;">Edit Akun Admin</font></b>
        </div>
        <div class="card" style="padding-top:10px">
            <div class="card-body">
            <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-12 mb-0">Username</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="Masukkan Username" value="<?= $pecah['username'] ?>" name="username" class="form-control ps-0 form-control-line" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Nama Lengkap</label>
                    <div class="col-md-12">
                        <input type="text" name="nama_lengkap" value="<?= $pecah['nama_lengkap'] ?>" placeholder="Masukkan Nama Lengkap" class="form-control ps-0 form-control-line" required >
                    </div>
                </div>                   
                <div class="form-group">
                    <label class="col-md-12 mb-0">Alamat</label>
                    <div class="col-md-12">
                        <textarea rows="5" name="alamat" class="form-control ps-0 form-control-line" required><?= $pecah['alamat'] ?></textarea>
                    </div>
                </div>                    
                <div class="form-group">
                    <div class="col-sm-12 d-flex">
                        <button class="btn btn-success mx-auto mx-md-0 text-white" name="save">Edit akun</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['save'])){
        $username       = $_POST['username'];
        $nama_lengkap   = $_POST['nama_lengkap'];
        $alamat         = $_POST['alamat'];
        
        $update = $koneksi->query("UPDATE admin
        SET username='$username', nama_lengkap='$nama_lengkap', alamat='$alamat'
        WHERE id_admin = '$id_admin'
        ");
        if($update){
            echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di Edit!', 'success');</script>" ;
            echo '<meta http-equiv="Refresh" content="3; URL=?page=akun">';
           
        }else{
            echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
            echo '<meta http-equiv="Refresh" content="3; URL=?page=akun">';
                                           
        }
    }
?>