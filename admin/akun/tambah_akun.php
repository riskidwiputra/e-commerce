<!-- Dashboard -->
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Tambah Akun</li>
            </ol>
        </div>
    </div>
    <!-- content -->
    <div class="card">
        <div class="card-header">
            <b><font style="font-weight: bold; font-size: 20px;">Akun Admin</font></b>
        </div>
        <div class="card" style="padding-top:10px">
            <div class="card-body">
            <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-12 mb-0">Username</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="Masukkan Username" name="username" class="form-control ps-0 form-control-line" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Password</label>
                    <div class="col-md-12">
                        <input type="password" name="password" placeholder="Masukkan Password" class="form-control ps-0 form-control-line" required >
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Nama Lengkap</label>
                    <div class="col-md-12">
                        <input type="text" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control ps-0 form-control-line" required >
                    </div>
                </div>                   
                <div class="form-group">
                    <label class="col-md-12 mb-0">Alamat</label>
                    <div class="col-md-12">
                        <textarea rows="5" name="alamat" class="form-control ps-0 form-control-line" required></textarea>
                    </div>
                </div>                    
                <div class="form-group">
                    <div class="col-sm-12 d-flex">
                        <button class="btn btn-success mx-auto mx-md-0 text-white" name="save">Tambah akun</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['save'])){
        $username       = $_POST['username'];
        $password       = $_POST['password'];
        $nama_lengkap   = $_POST['nama_lengkap'];
        $alamat         = $_POST['alamat'];
        $created        = date("Y-m-d H:i:s");
        $insert         = $koneksi->query("INSERT INTO admin
            (username,password,nama_lengkap,alamat,created_at)
            VALUES ('$username','$password','$nama_lengkap','$alamat','$created')");
        if($insert){
			echo"<script language='javascript'>swal ('Selamat...', 'Data Berhasil di input!', 'success');</script>" ;
			echo"<meta http-equiv='Refresh' content='3; URL=?page=akun'>";

		}else{
			echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
 	 		echo"<meta http-equiv='Refresh' content='3; URL=?page=akun'>"; 
 	 
		}
    }
?>