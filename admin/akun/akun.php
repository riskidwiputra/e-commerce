


<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Akun</li>
      </ol>
    </div>
  </div>
  <!-- content -->
  <div class="card">
    <div class="card-header">
      Akun Admin
    </div>
    <div class="card">
      <div class="table-responsive">
        <div class="card-block">
          <table class="table table-bordered  table-stripped table-hover table-striped" id="datatables">
            <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Nama Admin</th>
                <th>Alamat</th>
                <th style="text-align: center;"><a href="?page=tambah_akun"><button class="btn btn-outline-success" ><span class="fa fa-plus-circle fa-spin"></span> Tambah Data </button></a></th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $ambil = $koneksi->query("SELECT * FROM admin");
                $no = 1;
                while($pecah = $ambil->fetch_assoc()){
              ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $pecah['username'] ?></td>
                <td><?= $pecah['nama_lengkap'] ?></td>
                <td><?= $pecah['alamat'] ?></td>
                <td style="text-align: center;">
                  <!-- Edit -->
                  <a href="?page=edit_akun&id=<?= $pecah['id_admin'] ?>" style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg editTombol"  >
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </a>
                  &nbsp;
                  <!-- Hapus-->
                  <a href="?page=hapus_akun&id=<?= $pecah['id_admin'] ?>" style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-danger btn-lg delete-link" >
                    <i class="fa fa-trash-o"></i>
                  </a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>




<!-- Penutup -->
</div>
