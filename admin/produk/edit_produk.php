<?php 
    $id_produk = $_GET['id'];
    $ambil = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id_produk'");
    $pecah = $ambil->fetch_assoc();
    print_r($id_produk)
?>

<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Edit  Produk</li>
      </ol>
    </div>
  </div>
    <!-- content --><div class="card">
                        <div class="card-header">
                            <b><font style="font-weight: bold; font-size: 20px;">Produk</font></b>
                            </div>
                        <div class="card" style="padding-top:10px">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Nama Produk</label>
                                        <div class="col-md-12">
                                         
                                            <input type="text" placeholder="Masukkan Nama Produk" value="<?= $pecah['nama_produk'] ?>" name="name" class="form-control ps-0 form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Harga Produk</label>
                                        <div class="col-md-12">
                                            <input type="number" name="harga" value="<?= $pecah['harga_produk'] ?>" placeholder="Masukkan Harga Produk" class="form-control ps-0 form-control-line" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="example-email" class="col-md-12">Berat Produk</label>
                                        <div class="col-md-12">
                                            <input type="number" name="berat" value="<?= $pecah['berat'] ?>" placeholder="Masukkan Berat Produk" class="form-control ps-0 form-control-line" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <img src="images/<?= $pecah['foto']  ?>" width="150px" height="150px" alt="">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="example-email" class="col-md-12 mb-2">Foto</label>
                                        <div class="col-md-12 ">
                                            <input type="file" name="gambar" value="<?= $pecah['foto'] ?>" placeholder="Masukkan Gambar" class="form-control ps-0 form-control-line" >
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Deskripsi</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" name="deskripsi" class="form-control ps-0 form-control-line"><?= $pecah['deskripsi'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Pilih Ukuran</label>
                                        <div class="col-sm-12 border-bottom">
                                            <select class="form-select shadow-none ps-0 border-0 form-control-line" name="ukuran">
                                                <option value="<?= $pecah['size'] ?>"><?= strtoupper($pecah['size']); ?></option>
                                                <option value="x">X</option>
                                                <option value="xl">XL</option>
                                                <option value="xll">XLL</option>
                                                <option value="m">M</option>
                                                <option value="l">L</option>
                                                <option value="s">S</option>
                                                <option value="xxxl">XXXL</option>
                                                <option value="xxl">XXL</option>
                                                <option value="xs">XS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Pilih Warna</label>
                                        <div class="col-sm-12 border-bottom">
                                            <select class="form-select shadow-none ps-0 border-0 form-control-line" name="warna">
                                                <option value="<?= $pecah['warna'] ?>"><?= strtoupper($pecah['warna']); ?></option>
                                                <option value="hitam">Hitam</option>
                                                <option value="putih">Putih</option>
                                                <option value="merah">Merah</option>
                                                <option value="biru">Biru</option>
                                                <option value="pink">Pink</option>
                                                <option value="abu-abu">Abu-abu</option>
                                                <option value="hijau">Hijau</option>
                                                <option value="kuning_tua">kuning Tua</option>
                                                <option value="kuning_muda">Kuning Muda</option>
                                                <option value="ungu">Ungu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <button class="btn btn-success mx-auto mx-md-0 text-white" name="ubah">Ubah Produk</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['ubah'])){
        $foto           = $_FILES['gambar']['name'];
        $lokasi_foto    = $_FILES['gambar']['tmp_name'];
        $nama           = $_POST['name'];
        $harga          = $_POST['harga'];
        $berat          = $_POST['berat'];
        $deskripsi      = $_POST['deskripsi'];
        $ukuran         = $_POST['ukuran'];
        $warna          = $_POST['warna'];
        $lokasiFotod    = "images/".$pecah['foto']; 
        if(!empty($foto)){
            unlink($lokasiFotod);
            move_uploaded_file($lokasi_foto,"images/".$foto);
            
        }
        else{
            $foto = $lokasiFotod;
        }
            $update = $koneksi->query("UPDATE produk
            SET nama_produk='$nama', harga_produk='$harga', berat='$berat', foto='$foto', deskripsi='$deskripsi', size='$ukuran', warna='$warna'
            WHERE id_produk = '$id_produk'
            ");
            if($update){
                echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di Edit!', 'success');</script>" ;
                echo '<meta http-equiv="Refresh" content="3; URL=?page=produk">';
               
            }else{
                echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
                echo '<meta http-equiv="Refresh" content="3; URL=?page=produk">';
                                               
            }
       
      
    }
?>
