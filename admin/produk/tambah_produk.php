<!-- Dashboard -->

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Tambah Produk</li>
      </ol>
    </div>
  </div>
    <!-- content --><div class="card">
                        <div class="card-header">
                            <b><font style="font-weight: bold; font-size: 20px;">Produk</font></b>
                            </div>
                        <div class="card" style="padding-top:10px">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Nama Produk</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Masukkan Nama Produk" name="name" class="form-control ps-0 form-control-line" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Harga Produk</label>
                                        <div class="col-md-12">
                                            <input type="number" name="harga" placeholder="Masukkan Harga Produk" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="example-email" class="col-md-12">Berat Produk</label>
                                        <div class="col-md-12">
                                            <input type="number" name="berat" placeholder="Masukkan Berat Produk" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="example-email" class="col-md-12 mb-2">Foto</label>
                                        <div class="col-md-12 ">
                                            <input type="file" name="gambar" placeholder="Masukkan Gambar" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Deskripsi</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" name="deskripsi" class="form-control ps-0 form-control-line" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Pilih Ukuran</label>
                                        <div class="col-sm-12 border-bottom">
                                            <select class="form-select shadow-none ps-0 border-0 form-control-line" name="ukuran" required>
                                                <option hidden>Pilih Ukuran</option>
                                                <option value="x">X</option>
                                                <option value="xl">XL</option>
                                                <option value="xll">XLL</option>
                                                <option value="m">M</option>
                                                <option value="l">L</option>
                                                <option value="s">S</option>
                                                <option value="xxxl">XXXL</option>
                                                <option value="xxl">XXL</option>
                                                <option value="xs">XS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Pilih Warna</label>
                                        <div class="col-sm-12 border-bottom">
                                            <select class="form-select shadow-none ps-0 border-0 form-control-line" name="warna" required   >
                                                <option hidden>Pilih Warna</option>
                                                <option value="hitam">Hitam</option>
                                                <option value="putih">Putih</option>
                                                <option value="merah">Merah</option>
                                                <option value="biru">Biru</option>
                                                <option value="pink">Pink</option>
                                                <option value="abu-abu">Abu-abu</option>
                                                <option value="hijau">Hijau</option>
                                                <option value="kuning_tua">kuning Tua</option>
                                                <option value="kuning_muda">Kuning Muda</option>
                                                <option value="ungu">Ungu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-6">Pilih Kategori</label>
                                        <div class="col-sm-6 border-bottom">
                                            <select class="form-select shadow-none ps-0 border-0 form-control-line" name="kategori" onchange="getKategori(this);" required  >
                                                <option hidden>Pilih kategori</option>
                                                <?php 
                                                    $getkategori = $koneksi->query("SELECT * FROM kategori");
                                                    while($rowsKategori = $getkategori->fetch_assoc()){
                                                ?>
                                                <option value="<?= $rowsKategori['id_kategori'] ?>"><?= $rowsKategori['nama_kategori'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div> 
                                    </div>
                                    <div id="hidden_sub">
                                        <div class="form-group">
                                            <label class="col-sm-6">Pilih Sub Kategori</label>
                                            <div class="col-sm-6 border-bottom">
                                                <select class="form-select shadow-none ps-0 border-0 form-control-line" id="sub_kategori" name="sub_kategori">
                                                    
                                                    
                                                </select>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <button class="btn btn-success mx-auto mx-md-0 text-white" name="save">Tambah Produk</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['save'])){
        var_dump($_POST);
        $foto           = $_FILES['gambar']['name'];
        $lokasi_foto    = $_FILES['gambar']['tmp_name'];
        $nama           = $_POST['name'];
        $harga          = $_POST['harga'];
        $berat          = $_POST['berat'];
        $deskripsi      = $_POST['deskripsi'];
        $ukuran         = $_POST['ukuran'];
        $warna          = $_POST['warna'];
        $id_kategori    = $_POST['kategori'];
        if(!empty($_POST['sub_kategori'])){
            $id_sub_kategori = $_POST['sub_kategori'];
        }else{
            $id_sub_kategori = '';
        }
        $created        = date("d-m-Y H:i:s");
        move_uploaded_file($lokasi_foto,"images/".$foto);
        $insert = $koneksi->query("INSERT INTO produk
            (nama_produk,harga_produk,berat,foto,deskripsi,id_kategori,id_sub_kategori,size,warna,created_at)
            VALUES ('$nama','$harga','$berat','$foto','$deskripsi','$id_kategori','$id_sub_kategori','$ukuran','$warna','$created')");
        // echo " <div class='alert alert-info'>Data Tersimpan</div>";
        // echo "<meta http-equiv='refresh' content='1'>";
        if($insert){
			echo"<script language='javascript'>swal ('Selamat...', 'Data Berhasil di input!', 'success');</script>" ;
			echo"<meta http-equiv='Refresh' content='3; URL=?page=produk'>";

		}else{
			echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
 	 		echo"<meta http-equiv='Refresh' content='3; URL=?page=produk'>"; 
 	 
		}
    }
?>
<script>
    $("#hidden_sub").hide();
    function getKategori(params){
        $("#hidden_sub").show();
        var get_id_kategori  = params.value;
        var date_add = '<option hidden>Pilih Sub Kategori</option>';
        $.ajax({
        url:'produk/getDataKategori.php',
        method: 'POST',
        dataType: 'JSON',
        data:{id : get_id_kategori},
        success:function(data){
           
            $.each(data,function(index,obj)
            {
                date_add += `<option  value="`+obj.id_sub_kategori +`">`+ obj.nama +`</option>`;
            });
            $("#sub_kategori").html(date_add);
        }
      });
    }
</script>