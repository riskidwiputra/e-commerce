
<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">produk</li>
      </ol>
    </div>
  </div>

  <!-- Content Menampilakan produk -->
  <div class="col-lg-12 col-xl-12 col-md-12 "   >
    <div class="card">
      <div class="table-responsive">
        <div class="card-block">
          <table class="table table-bordered table-hover table-striped" id="datatables">
            <thead>
              <tr>
                <th width="5%" >No</th>
                <th width="14%" >Nama </th>
                <th width="15%" >harga </th>
                <th width="15%" >Berat </th>
                <th width="15%" >Foto </th>
                <th width="10%">Size</th>
                <th  width="12%">Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $ambil = $koneksi->query("SELECT * FROM produk");
                $no = 1;
                while($pecah = $ambil->fetch_assoc()){
              ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $pecah['nama_produk'] ?></td>
                <td><?= $pecah['harga_produk'] ?></td>
                <td><?= $pecah['berat'] ?></td>
                <td><img src="images/<?= $pecah['foto'] ?>" width="100" height="100" alt="" /></td>
                <td><?=  $pecah['size'] ?></td>
                <td align="center" >
                  <a href="?page=edit_produk&id=<?= $pecah['id_produk'] ?>"style="padding:3px 9px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg ">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </a>
                  <a href="?page=hapus_produk&id=<?= $pecah['id_produk'] ?>" style="padding:3px 9px; text-decoration:none; color:#111;" class="btn btn-3d btn-danger btn-lg delete-link">
                    <i class="fa fa-trash-o"></i>
                  </a>
                </td>
              </tr>
            </tbody>
            <?php } ?>
          </table>
            <!-- Tambah produk -->
          <a href="?page=tambah_produk"><button  class="btn btn-success"  ><span class="fa fa-plus-circle fa-spin"></span> Tambah Data</button></a>
        </div>
      </div>
    </div>
  </div>
<!-- Penutup -->  
</div>

