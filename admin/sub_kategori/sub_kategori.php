


<?php 
    if(isset($_GET['id'])){
        $id_kategori = $_GET['id'];
    }else{
        echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
        echo"<meta http-equiv='Refresh' content='3; URL=?page=kategori'>";
    }
    // Tambah kategori 
    if(isset($_POST['sub_kategori'])){
        $nama_sub_kategori  = $_POST['nama_sub_kategori'];
        $foto               = $_FILES['gambar']['name'];
        $source             = $_FILES['gambar']['tmp_name'];
        $date               = date("Y-m-d H:i:s");
        move_uploaded_file($source,"images/".$foto);
        $insert = $koneksi->query("INSERT INTO sub_kategori
            (id_kategori,nama,gambar,created_at)
            VALUES ('$id_kategori','$nama_sub_kategori','$foto','$date')");
        if($insert){
        echo"<script language='javascript'>swal ('Selamat...', 'Data Berhasil di input!', 'success');</script>" ;
        echo"<meta http-equiv='Refresh' content='3; URL=?page=sub-kategori&id=".$id_kategori."'>";
    
        }else{
        echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
        echo"<meta http-equiv='Refresh' content='3; URL=?page=sub-kategori&id=".$id_kategori."'>"; 
    
        }
    } 
    if(isset($_POST['editsubkategori'])){
      $foto               = $_FILES['gambar']['name'];
      $lokasi_foto        = $_FILES['gambar']['tmp_name'];
      $nama_sub_kategori  = $_POST['nama'];
      $id_sub_kategori    = $_POST['id_sub_kategori'];
      $gambarS            = $_POST['gambar_s'];
      $lokasiFotod        = "images/".$gambarS; 
      if(!empty($foto)){
        unlink($lokasiFotod);
        move_uploaded_file($lokasi_foto,"images/".$foto);
        $update = $koneksi->query("UPDATE sub_kategori
        SET nama='$nama_sub_kategori', gambar='$foto'
        WHERE id_sub_kategori   = '$id_sub_kategori'
        ");
      }else{
        $update = $koneksi->query("UPDATE sub_kategori
        SET nama='$nama_sub_kategori'
        WHERE id_sub_kategori  = '$id_sub_kategori'
        ");
      }
      
        if($update){
            echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di Edit!', 'success');</script>" ;
            echo "<meta http-equiv='Refresh' content='3; URL=?page=sub-kategori&id=".$id_kategori."'>"; 
            
        }else{
            echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
            echo "<meta http-equiv='Refresh' content='3; URL=?page=sub-kategori&id=".$id_kategori."'>"; 
                                            
        }
    };

    // Hapus kategori 
  if(isset($_GET['hapus'])){
    $id_sub_kategori = $_GET['hapus'];
    // var_dump($id_kategori);die;
    $ambil = $koneksi->query("SELECT * FROM sub_kategori WHERE id_sub_kategori  = '$id_sub_kategori'");
    $pecah = $ambil->fetch_assoc();
    $foto_produk = $pecah['gambar'];
    if(file_exists("images/$foto_produk")){
        unlink("images/$foto_produk");
    }
    $delete = $koneksi->query("DELETE FROM sub_kategori WHERE id_sub_kategori = '$id_sub_kategori'");
    if($delete){
        echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di hapus!', 'success');</script>" ;
   
        
    }else{
        echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
      
    }
  }
?>

<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item"><a href="?page=kategori">Kategori</a></li>
        <li class="breadcrumb-item active">Sub kategori</li>
      </ol>
    </div>
  </div>
  <!-- content -->
  <div class="card">
    <div class="card-header">
      Sub kategori
    </div>
    <div class="card">
      <div class="table-responsive">
        <div class="card-block">
          <table class="table table-bordered  table-stripped table-hover table-striped" id="datatables">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Sub Kategori</th>
                <th>Gambar</th>
                <th style="text-align: center;"><button class="btn btn-outline-success" data-toggle="modal" data-target="#TambahAdmin"><span class="fa fa-plus-circle fa-spin"></span> Tambah Data </button></th>
              </tr>
            </thead>
            <tbody>
              <!-- Menampilkan kategori -->
              <?php 
                $ambil = $koneksi->query("SELECT * FROM sub_kategori WHERE id_kategori = '$id_kategori'");
                $no = 1;
                if($ambil){
                while($row = $ambil->fetch_assoc()){
              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $row['nama']; ?></td>

                <td align="center"><img src="images/<?php echo $row['gambar']; ?>" width="150" height="50" alt="" /></td>
                <td style="text-align: center;">
                  
                  <!-- Edit -->
                  <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg kode_kategori"  id="<?= $row['id_sub_kategori']; ?>"   data-toggle="modal" data-target="#edit" >
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </a>
                &nbsp;
                <!-- Hapus-->
                <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-danger btn-lg delete-link" id="tombol" href="?page=sub-kategori&id=<?=$id_kategori?>&hapus=<?=$row['id_sub_kategori']; ?>">
                  <i class="fa fa-trash-o"></i>
                </a>
                </td>
              </tr>
                <?php 
               } 
              } 
              ?>


          
            

            <!-- Modal Edit kategori -->
            <div id="edit" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h4 class="modal-tittle">EDIT DATA Sub Kategori</h4>
                  </div> 
                  <div id="datas">
                 

                 
                   </div>
                </div>
              </div>
            </div>

            <!--Modal Tambah kategori -->
            <div class="modal fade" id="TambahAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Sub kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data" >
                      <div class="form-group">
                        <label for=""></span> Nama Sub Kategori</label>
                        <input type="text" name="nama_sub_kategori" class="form-control" placeholder="nama sub kategori">
                      </div>
                      <div class="form-group">
                        <label for=""></span>gambar</label>
                        <input type="file" name="gambar" class="form-control" placeholder="gambar">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary " data-dismiss="modal">Tutup</button>
                      <button type="submit" name="sub_kategori" class="btn btn-primary" >Tambah</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>




<!-- Penutup -->
</div>
<!-- Menampilakan modal edit kategori -->

<!-- Menampilakan Detail kategori -->

<script>
  $(document).ready(function(){
    $('.kode_kategori').on('click',function(){
      var id = $(this).attr("id");

      $.ajax({
        url:'sub_kategori/getData.php',
        method:'get',
        data:{id : id},
        success:function(data){
          $('#datas').html(data);
        }
      });
    });
  })
</script>


