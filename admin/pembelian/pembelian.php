
<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Pembelian</li>
      </ol>
    </div>
  </div>


  <!-- Content Menampilakan Pembelian -->
    <div class="col-lg-12 col-xl-12 col-md-12 "   >
      <div class="card">
        <div class="table-responsive">
          <div class="card-block">
            <table class="table table-bordered table-hover table-striped" id="datatables">
              <thead>
                <tr><th width="5%" >No</th>
                  <th width="16%" >pelanggan</th>
                  <th width="8%" >Ongkir</th>
                  <th width="15%" >Tanggal Pembelian</th>
                  <th width="15%" >Total Pembelian </th>
                  <th width="10%" class="text-center">Aksi</th>
                </tr>
              </thead>
                <tbody>
                  <!-- Menampilkan Pembelian -->
                  <?php
                  $no = 1;
                    $ambil = $koneksi->query("SELECT * FROM pembelian AS pb JOIN pelanggan AS pl ON pb.id_pelanggan = pl.id_pelanggan");
                    while ($row = $ambil->fetch_assoc()) {
                  ?>
                  
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row['nama_pelanggan']; ?></td>
                    <td><?php echo $row['tarif']; ?></td>
                    <td><?php echo $row['tanggal_pembelian']; ?></td>
                    <td><?=	"Rp. ".number_format($row['total_pembelian'],0,',','.').",-"; ?></td>
                    <td align="center">
                      <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg id_pembelian" id="<?= $row['id_pembelian']; ?>" 
                          data-toggle="modal" data-target=".detail">
                          <i class="fa  fa-search"> </i>
                      </a>
                    </td>
                  </tr>
                <?php }
                ?>
              </tbody>
            </table>
            <!-- .modal detail -->
              <div class="modal fade bd-example-modal-lg detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"></button>
                      <h3 class="modal-tittle">Detail Pembelian</h3>
                    </div> 
                    <div id="content_detail">
                    </div>
                  </div>
                </div>
              </div>
            <!-- .modal detail -->
          </div>
        </div>
      </div>
    </div>

<!-- Penutup -->  
</div>
<script>
  $(document).ready(function(){
    $('.id_pembelian').on('click',function(){
      var id = $(this).attr("id");
    
      $.ajax({
        url:'pembelian/getData.php',
        method:'get',
        data:{
          id : id
        },
        success:function(data){
          $('#content_detail').html(data);
        }
      });
    });
  })
</script>


