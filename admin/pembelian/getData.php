<?php 
if(isset($_GET['id'])){
  $id_pembelian = $_GET['id'];
  $koneksi = new mysqli("localhost","root","","ecommerce");
  $ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pelanggan.id_pelanggan=pelanggan.id_pelanggan WHERE pembelian.id_pembelian='$id_pembelian'");
  $data = $ambil->fetch_assoc();
}
 ?>
 <div class="container mb-4" >
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr >
                            <td width="25%" >Pembelian </td>
                            <td >: id pembelian </td>
                            <td colspan="2" > :  <?php echo $data['id_pembelian']; ?></td>            
                        </tr>
                        <tr>
                            <td></td>
                            <td width="35%"> Tanggal Pembelian</td>
                            <td colspan="2" >:  <?= $data['tanggal_pembelian']; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="35%"> Total Pembelian</td>
                            <td colspan="2" >:  <?= $data['total_pembelian']; ?></td>
                        </tr>
                        <tr>  
                            <td width="25%"> Pelanggan</td>
                            <td>: id pelanggan</td>
                            <td colspan="2" > :  <?php echo $data['id_pelanggan']; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="35%"> Nama Pelanggan</td>
                            <td colspan="2" >:  <?= $data['nama_pelanggan']; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="35%"> Email</td>
                            <td colspan="2" >:  <?= $data['email_pelanggan']; ?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td width="35%"> Alamat</td>
                            <td colspan="2" >:  <?= $data['alamat']; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td width="35%"> Nomor Handphone</td>
                            <td colspan="2" >:  <?= $data['telepon']; ?></td>
                        </tr>
                         
                    </tbody>
                </table>
                <h2>Detail Produk</h2>
                <table class="table table-striped">
                    <thead>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Berat</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Sub Total</th>
                    </thead>
                    <tbody>
                    <?php 
                    $no=1;
                        $getProduk = $koneksi->query("SELECT * FROM pembelian_produk AS pp JOIN produk AS p ON pp.id_produk=p.id_produk WHERE pp.id_pembelian='$id_pembelian' ");
                        while ($rows = $getProduk->fetch_assoc()) {
                    ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $rows['nama_produk'] ?></td>
                            <td><?= $rows['berat'] ?></td>
                            <td><?= $rows['harga_produk'] ?></td>
                            <td><?= $rows['jumlah'] ?></td>
                            <td><?= $rows['harga_produk'] * $rows['jumlah'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>