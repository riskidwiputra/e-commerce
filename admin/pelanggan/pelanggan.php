

<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Pelanggan</li>
      </ol>
    </div>
  </div>


  <!-- content -->
  <div class="card">
    <div class="card-header">
      <b><font style="font-weight: bold; font-size: 20px;">Pelanggan</font></b>
    </div>
    <div class="card">
      <div class="table-responsive">
        <div class="card-block">
          <table class="table table-bordered  table-stripped table-hover table-striped" id="datatables">
            <thead>
              <tr>
                <th  width="5%">No</th>
                <th  width="15%">Nama </th>
                <th  width="20%">Email</th>
                <th  width="20%">Telepon</th>
                <th  width="20%" style="text-align: center;"><button class="btn btn-outline-success" data-toggle="modal" data-target="#TambahCustomer"><span class="fa fa-plus-circle fa-spin"></span> Tambah Data </button></th>
              </tr>
            </thead>
            <tbody>
            <?php 
             $ambil = $koneksi->query("SELECT * FROM pelanggan");
             $no = 1;
             while($pecah = $ambil->fetch_assoc()){
            ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $pecah['nama_pelanggan'] ?></td>
                <td><?= $pecah['email_pelanggan'] ?></td>
                <td><?= $pecah['telepon'] ?></td>
                <td style="text-align: center;">
                  <!-- menampilkan Detail -->
                  <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg kode_customer"  
                    data-toggle="modal" data-target="#myModal">
                    <i class="fa  fa-search"> </i>
                  </a>
                  &nbsp;
                  <!-- Edit -->
                  <a href="?page=edit_pelanggan&id=<?= $pecah['id_pelanggan'] ?>" style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg editTombol"  data-toggle="modal" data-target="#edit" >
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </a>
                  &nbsp;
                  <!-- Hapus-->
                  <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-danger btn-lg delete-link" id="tombol" >
                    <i class="fa fa-trash-o"></i>
                  </a>
                </td>
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>




<!-- Penutup -->
</div>
