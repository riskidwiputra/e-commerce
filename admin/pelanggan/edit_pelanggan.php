
<?php 
    $id_pelanggan = $_GET['id'];
    $ambil = $koneksi->query("SELECT * FROM pelanggan WHERE id_pelanggan = '$id_pelanggan'");
    $pecah = $ambil->fetch_assoc();
?>
<!-- Dashboard -->

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Tambah Pelanggan</li>
      </ol>
    </div>
  </div>
    <!-- content --><div class="card">
                        <div class="card-header">
                            <b><font style="font-weight: bold; font-size: 20px;">Pelanggan</font></b>
                            </div>
                        <div class="card" style="padding-top:10px">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Email Pelanggan</label>
                                        <div class="col-md-12">
                                            <input type="email" placeholder="Masukkan Email Pelanggan" name="email" value="<?= $pecah['email_pelanggan'] ?>" class="form-control ps-0 form-control-line" required>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                    <label for="example-email" class="col-md-12">Nama Pelanggan</label>
                                        <div class="col-md-12">
                                            <input type="text" name="nama_pelanggan" placeholder="Masukkan Nama Pelanggan" value="<?= $pecah['nama_pelanggan'] ?>" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="example-email" class="col-md-12 mb-2">Telepon</label>
                                        <div class="col-md-12 ">
                                            <input type="number" name="telepon" placeholder="Masukkan telepon" value="<?= $pecah['telepon'] ?>" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Alamat</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" name="alamat" class="form-control ps-0 form-control-line" required><?= $pecah['alamat'] ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <button class="btn btn-success mx-auto mx-md-0 text-white" name="edit">Edit Pelanggan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['edit'])){
        $email              = $_POST['email'];
        $nama_pelanggan     = $_POST['nama_pelanggan'];
        $telepon            = $_POST['telepon'];
        $alamat             = $_POST['alamat'];
        $id_pelanggan       = $pecah['id_pelanggan'];
        $update = $koneksi->query("UPDATE pelanggan
        SET 	email_pelanggan='$email', nama_pelanggan='$nama_pelanggan', telepon='$telepon', alamat='$alamat'
        WHERE id_pelanggan = '$id_pelanggan'
        ");
        if($update){
            echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di Edit!', 'success');</script>" ;
            echo '<meta http-equiv="Refresh" content="3; URL=?page=pelanggan">';
           
        }else{
            echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
            echo '<meta http-equiv="Refresh" content="3; URL=?page=pelanggan">';
                                           
        }
    }
?>

