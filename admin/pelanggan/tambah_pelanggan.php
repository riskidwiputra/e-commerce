<!-- Dashboard -->

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">Edit Pelanggan</li>
      </ol>
    </div>
  </div>
    <!-- content --><div class="card">
                        <div class="card-header">
                            <b><font style="font-weight: bold; font-size: 20px;">Pelanggan</font></b>
                            </div>
                        <div class="card" style="padding-top:10px">
                            <div class="card-body">
                                <form class="form-horizontal form-material mx-2" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Email Pelanggan</label>
                                        <div class="col-md-12">
                                            <input type="email" placeholder="Masukkan Email Pelanggan" name="email" class="form-control ps-0 form-control-line" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" name="password" placeholder="Masukkan Password" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="example-email" class="col-md-12">Nama Pelanggan</label>
                                        <div class="col-md-12">
                                            <input type="text" name="nama_pelanggan" placeholder="Masukkan Nama Pelanggan" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="example-email" class="col-md-12 mb-2">Telepon</label>
                                        <div class="col-md-12 ">
                                            <input type="number" name="telepon" placeholder="Masukkan telepon" class="form-control ps-0 form-control-line" required >
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-12 mb-0">Alamat</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" name="alamat" class="form-control ps-0 form-control-line" required></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12 d-flex">
                                            <button class="btn btn-success mx-auto mx-md-0 text-white" name="save">Tambah Pelanggan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    <!-- content -->
</div>
<?php 
    if(isset($_POST['save'])){
        $email           = $_POST['email'];
        $password          = $_POST['password'];
        $nama_pelanggan          = $_POST['nama_pelanggan'];
        $telepon      = $_POST['telepon'];
        $alamat         = $_POST['alamat'];
    
        $insert = $koneksi->query("INSERT INTO pelanggan
            (email_pelanggan,password_pelanggan,nama_pelanggan,telepon,alamat)
            VALUES ('$email','$password','$nama_pelanggan','$telepon','$alamat')");
        if($insert){
			echo"<script language='javascript'>swal ('Selamat...', 'Data Berhasil di input!', 'success');</script>" ;
			echo"<meta http-equiv='Refresh' content='3; URL=?page=pelanggan'>";

		}else{
			echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
 	 		echo"<meta http-equiv='Refresh' content='3; URL=?page=pelanggan'>"; 
 	 
		}
    }
?>
