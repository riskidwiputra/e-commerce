<?php 

// Tambah kategori 
if(isset($_POST['kategori'])){
  $nama_kategori  = $_POST['nama_kategori'];
  $foto           = $_FILES['gambar']['name'];
  $source         = $_FILES['gambar']['tmp_name'];
  $date           = date("Y-m-d H:i:s");
  move_uploaded_file($source,"images/".$foto);
  $insert = $koneksi->query("INSERT INTO kategori
      (nama_kategori,	gambar,created_at)
      VALUES ('$nama_kategori','$foto','$date')");
    if($insert){
    echo"<script language='javascript'>swal ('Selamat...', 'Data Berhasil di input!', 'success');</script>" ;
    echo"<meta http-equiv='Refresh' content='3; URL=?page=kategori'>";

    }else{
    echo"<script language='javascript> swal('Oops...', 'Something went wrong!', 'error');";
    echo"<meta http-equiv='Refresh' content='3; URL=?page=kategori'>"; 

    }
} 
?>

<?php

// Edit kategori
  if(isset($_POST['editkategori'])){
    $foto           = $_FILES['gambar']['name'];
    $lokasi_foto    = $_FILES['gambar']['tmp_name'];
    $nama_kategori  = $_POST['nama_kategori'];
    $id_kategori    = $_POST['id_kategori'];
    $gambarS        = $_POST['gambar_s'];
    $lokasiFotod    = "images/".$gambarS; 
    if(!empty($foto)){
      unlink($lokasiFotod);
      move_uploaded_file($lokasi_foto,"images/".$foto);
      $update = $koneksi->query("UPDATE kategori
      SET nama_kategori='$nama_kategori', gambar='$foto'
      WHERE id_kategori  = '$id_kategori'
      ");
    }else{
      $update = $koneksi->query("UPDATE kategori
      SET nama_kategori='$nama_kategori'
      WHERE id_kategori  = '$id_kategori'
      ");
    }
    
      if($update){
          echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di Edit!', 'success');</script>" ;
          echo '<meta http-equiv="Refresh" content="3; URL=?page=kategori">';
          
      }else{
          echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
          echo '<meta http-equiv="Refresh" content="3; URL=?page=kategori">';
                                          
      }
  };

// Hapus kategori 
if(isset($_GET['hapus'])){
  $id_kategori = $_GET['hapus'];
  // var_dump($id_kategori);die;
  $ambil = $koneksi->query("SELECT * FROM kategori WHERE id_kategori = '$id_kategori'");
  $pecah = $ambil->fetch_assoc();
  $foto_produk = $pecah['gambar'];
  if(file_exists("images/$foto_produk")){
      unlink("images/$foto_produk");
  }
  $delete = $koneksi->query("DELETE FROM kategori WHERE id_kategori = '$id_kategori'");
  if($delete){
      echo "<script language='javascript'>swal('Selamat...', 'Data Berhasil di hapus!', 'success');</script>" ;
        echo '<meta http-equiv="Refresh" content="3; URL=?page=kategori">';
      
  }else{
      echo "<script language='javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>" ;
      echo '<meta http-equiv="Refresh" content="3; URL=?page=kategori">';
                                      
  }
}

?>





<!-- Dashboard -->
<div class="container-fluid">
  <div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
      <h3 class="text-themecolor">Dashboard</h3>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active">kategori</li>
      </ol>
    </div>
  </div>
  <!-- content -->
  <div class="card">
    <div class="card-header">
      Akun kategori
    </div>
    <div class="card">
      <div class="table-responsive">
        <div class="card-block">
          <table class="table table-bordered  table-stripped table-hover table-striped" id="datatables">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kategori</th>
                <th>Gambar</th>
                <th style="text-align: center;"><button class="btn btn-outline-success" data-toggle="modal" data-target="#TambahAdmin"><span class="fa fa-plus-circle fa-spin"></span> Tambah Data </button></th>
              </tr>
            </thead>
            <tbody>
              <!-- Menampilkan kategori -->
              <?php 
                $ambil = $koneksi->query("SELECT * FROM kategori");
                $no = 1;
                if($ambil){
                while($row = $ambil->fetch_assoc()){
              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $row['nama_kategori']; ?></td>

                <td align="center"><img src="images/<?php echo $row['gambar']; ?>" width="150" height="50" alt="" /></td>
                <td style="text-align: center;">
                  <!-- menampilkan Detail -->
                  <a  href="?page=sub-kategori&id=<?= $row['id_kategori']; ?>" style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg"   >
                    <i class="fa fa-sign-in" > Sub kategori </i>
                  </a>
                  &nbsp;
                  <!-- Edit -->
                  <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-info btn-lg kode_kategori"  id="<?= $row['id_kategori']; ?>"   data-toggle="modal" data-target="#edit" >
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </a>
                &nbsp;
                <!-- Hapus-->
                <a style="padding:5px 12px; text-decoration:none; color:#111;" class="btn btn-3d btn-danger btn-lg delete-link" id="tombol" href="?page=kategori&hapus=<?=$row['id_kategori']; ?>">
                  <i class="fa fa-trash-o"></i>
                </a>
                </td>
              </tr>
                <?php 
               } 
              } 
              ?>


          
            

            <!-- Modal Edit kategori -->
            <div id="edit" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h4 class="modal-tittle">EDIT DATA kategori</h4>
                  </div> 
                  <div id="datas">
                 

                 
                   </div>
                </div>
              </div>
            </div>

            <!--Modal Tambah kategori -->
            <div class="modal fade" id="TambahAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data" >
                      <div class="form-group">
                        <label for=""></span> nama_kategori</label>
                        <input type="text" name="nama_kategori" class="form-control" placeholder="nama_kategori">
                      </div>
                      <div class="form-group">
                        <label for=""></span>gambar</label>
                        <input type="file" name="gambar" class="form-control" placeholder="gambar">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary " data-dismiss="modal">Tutup</button>
                      <button type="submit" name="kategori" class="btn btn-primary" >Tambah</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>




<!-- Penutup -->
</div>
<!-- Menampilakan modal edit kategori -->

<!-- Menampilakan Detail kategori -->

<script>
  $(document).ready(function(){
    $('.kode_kategori').on('click',function(){
      var id = $(this).attr("id");

      $.ajax({
        url:'kategori/getData.php',
        method:'get',
        data:{id : id},
        success:function(data){
          $('#datas').html(data);
        }
      });
    });
  })
</script>


