<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/cart-variant1.html   11 Nov 2019 12:44:31 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Cart Page &ndash; Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle cart-variant1">
<div class="pageWrapper">
	<!--Search Form Drawer-->
    <!--End Search Form Drawer-->
     <!-- header -->
     <?php include "header.php" ?>
    <!-- header -->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Shopping Cart</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<!-- <div class="alert alert-success text-uppercase" role="alert">
						<i class="icon anm anm-truck-l icon-large"></i> &nbsp;<strong>Congratulations!</strong> You've got free shipping!
					</div> -->
                	<form action="#" method="post" class="cart style2">
                		<table>
                            <thead class="cart__row cart__header">
                                <tr>
                                    <th  class="text-center">No</th>
                                    <th colspan="2" class="text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-right">Total</th>
                                  
                                </tr>
                            </thead>
                    		<tbody id="data_keranjang">
                                <?php 
                                if(isset($_SESSION['keranjang'])){
                                    $totalHarga = 0;
                                    $i=1; foreach($_SESSION['keranjang'] as $id_produk => $jumlah){ 
                                   
                                    $getDataProduk = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id_produk'");
                                    $rows = $getDataProduk->fetch_assoc();
                                ?>

                                <tr class="cart__row border-bottom line1 cart-flex border-top">
                                    <td class="text-center small--hide">
                                        <span class=""><?= $i ?></span>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <a href="#"><img class="cart__image" src="admin/images/<?= $rows['foto'] ?>" width="150px" height="130px" ></a>
                                    </td>
                                    <td class="cart__meta small--text-left cart-flex-item">
                                        <div class="list-view-item__title">
                                            <a href="#"><?= $rows['nama_produk'] ?></a>
                                        </div>
                                        
                                        <div class="cart__meta-text">
                                            Color: Navy<br>Size: Small<br>
                                        </div>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item">
                                        <span class="money"><?=	"Rp. ".number_format($rows['harga_produk'],0,',','.').",-"; ?></span>
                                    </td>
                                    <td class="cart__update-wrapper cart-flex-item text-right">
                                        <div class="cart__qty text-center">
                                            <div class="qtyField">
                                                <a class="qtyBtn minus" href="javascript:void(0);" onclick="kurang_keranjang()"> <i class="icon icon-minus"></i></a>
                                                    <input class="cart__qty-input qty" name="jumlah_produk" type="text"  id="qtykeranjang<?= $i ?>" value="<?= $jumlah ?>" pattern="[0-9]*">
                                                <a class="qtyBtn plus" href="javascript:void(0);"  ><i class="icon icon-plus"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <?php $harga_produk = $rows['harga_produk'] * $jumlah;  ?>
                                        <div><span class="money"><?=	"Rp. ".number_format($harga_produk,0,',','.').",-"; ?></span></div>
                                    </td>
                                
                                </tr>
                                <?php $i++; $totalHarga += $harga_produk; }
                                 }else{ ?>
                                     <tr class="cart__row border-bottom line1 cart-flex border-top"><td colspan="7" class="text-center small--hide mt-3 mb-3"><h2 class="mt-3 mb-3" >Keranjang Kosong</h2></td></tr>

                                 <?php } ?>
                            </tbody>
                    		<tfoot>
                                <tr>
                                  
                                    <td colspan="7" class="text-left"><a href="keranjang.php" class="btn btn-secondary btn--large cart-continue">Back</a></td>
                                    
                                </tr>
                            </tfoot>
                    </table> 
                    </form>                   
               	</div>
                
                
                <div class="container mt-4">
                <form action="#" method="post">
                    <div class="row">
                    
                    	<div class="col-12 col-sm-12 col-md-4 col-lg-4 mb-4">
                        	<h5>Data Pelanggan</h5>
                            
                            	<div class="form-group">
                                    <label for="address_zip">Nama Pelanggan</label>
                                    <input type="text"  value="<?= $_SESSION['pelanggan']['nama_pelanggan'] ?>" readonly class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="address_zip">No Handphone</label>
                                    <input type="text"  value="<?= $_SESSION['pelanggan']['telepon'] ?>" readonly class="form-control">
                                </div>
                                
                               
                           
                        </div>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 mb-4">
                           
                        	<h5>Pilih Ongkos Kirim</h5>
						
                                <div class="form-group">
                                    <label for="address_country">Ongkir</label>
                                    <select id="address_country" class="select_ongkir" name="ongkir" data-default="United States" required>
                                    <option hidden >Pilih Ongkos Kirim</option>
                                    <?php 
                                        $getDataOngkir = $koneksi->query("SELECT * FROM ongkir");
                                        while($rowOnkir = $getDataOngkir->fetch_assoc()){
                                    ?>
                                    <option value="<?= $rowOnkir['id_ongkir']?>" data-id="<?= $rowOnkir['tarif'] ?>" ><?= $rowOnkir['nama_kota'] ?> <?=" - Rp. ".number_format($rowOnkir['tarif'],0,',','.').",-"; ?></option>

                                    <?php } ?>
                                   </select>
                                </div>
                                <div class="cart-note">
                                    <div class="solid-border">
                                        <h5><label for="CartSpecialInstructions" class="cart-note__label small--text-center">Masukkan Alamat Pengirim</label></h5>
                                        <textarea name="alamat_pengirim" id="CartSpecialInstructions" class="cart-note__input"></textarea>
                                    </div>
                                </div>
                                
    
                              
                        </div>
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 cart__footer">
                            <div class="solid-border">	
                              <div class="row border-bottom pb-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title">Subtotal</span>
                                <span class="col-12 col-sm-6 text-right"><span class="money"><?=	"Rp. ".number_format($totalHarga,0,',','.').",-"; ?></span></span>
                              </div>
                              <div class="row border-bottom pb-2 pt-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title">Ongkir</span>
                                <span class="col-12 col-sm-6 text-right" id="ongkir"></span>
                              </div>
                              <div class="row border-bottom pb-2 pt-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title">Shipping</span>
                                <span class="col-12 col-sm-6 text-right">Free shipping</span>
                              </div>
                              <div class="row border-bottom pb-2 pt-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title"><strong>Grand Total</strong></span>
                                <span class="col-12 col-sm-6 cart__subtotal-title cart__subtotal text-right"><span class="money" id="grand_total"></span></span>
                              </div>
                              <div class="cart__shipping">Shipping &amp; taxes calculated at checkout</div>
                              <p class="cart_tearm">
                                <label>
                                  <input type="checkbox"  class="checkbox" value="tearm" required="">
                                  I agree with the terms and conditions
								</label>
                              </p>
                         
                              <button  name="checkout" id="cartCheckout" class="btn btn--small-wide checkout btn-success">Proceed To Checkout</button>
                              <div class="paymnet-img"><img src="assets/images/payment-img.jpg" alt="Payment"></div>
                              <p><a href="#;">Checkout with Multiple Addresses</a></p>
                            </div>
        
                        </div>
                  
                    </div>
                </div>
                </form>
                <?php if (isset($_POST['checkout'])){
                    $id_pelanggan = $_SESSION['pelanggan']['id_pelanggan'];
                    $id_ongkir  = $_POST['ongkir'];
                    $alamat     = $_POST['alamat_pengirim'];
                    $tanggal_pembelian =  date("Y-m-d H:i:s");

                    $getDataOngkirs = $koneksi->query("SELECT * FROM ongkir WHERE id_ongkir = '$id_ongkir'");
                    $ongkir         = $getDataOngkirs->fetch_assoc();
                    $totalkeseluruhan   = $ongkir['tarif'] + $totalHarga;
                    $tarif              = $ongkir['tarif'];
                    $nama_kota          = $ongkir['nama_kota'];
                 // var_dump($totalkeseluruhan); 
                    $koneksi->query("INSERT INTO pembelian (id_pelanggan,tanggal_pembelian,total_pembelian,id_ongkir,nama_kota,tarif,alamat_pengiriman) VALUES ('$id_pelanggan','$tanggal_pembelian','$totalkeseluruhan','$id_ongkir','$nama_kota','$tarif','$alamat')");
                    $id_pembelian = $koneksi->insert_id;
                    foreach ($_SESSION['keranjang'] as $id_produk => $jumlah) {
                        $getProduk  = $koneksi->query("SELECT * FROM produk WHERE id_produk='$id_produk'");
                        $dataproduk = $getProduk->fetch_assoc();
                        $nama       = $dataproduk['nama_produk'];
                        $harga      = $dataproduk['harga_produk'];
                        $berat      = $dataproduk['berat'];
                        $subberat   = $berat * $jumlah;
                        $subharga   = $harga * $jumlah;
                        
                        
                        $updatePorudk = $koneksi->query("UPDATE produk SET terjual='$jumlah' WHERE id_produk = '$id_produk'");
                        $beli = $koneksi->query("INSERT INTO pembelian_produk (id_pembelian,id_produk,jumlah,nama,harga,berat,sub_berat,sub_harga) VALUES ('$id_pembelian','$id_produk','$jumlah','$nama','$harga','$berat','$subberat','$subharga')");
                    }
                    unset($_SESSION['keranjang']);
                    echo "<script>alert('Pembelian Sukses');</script>";
                    echo "<script>location='nota.php?id=$id_pembelian'</script>";
                }
                ?>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php include "footer.php" ?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>
<script>

$(document).ready(function(){
	$('.select_ongkir').change(function(){
  		var getData = $(this).find("option:selected").data('id');
            parseInt(getData);
        var ganti   = formatRupiah(String(getData), 'Rp. ');
        var totalHargas = parseInt(<?= $totalHarga ?>);
        let grandTotal  = parseInt(getData)+parseInt(totalHargas);
        let convertgrandTotal  = formatRupiah(String(grandTotal), 'Rp. ');
  		$('#ongkir').html(ganti);
        $('#grand_total').html(convertgrandTotal);
  	});
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
});
</script>

</html>