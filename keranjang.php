<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/cart-variant1.html   11 Nov 2019 12:44:31 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Cart Page &ndash; Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle cart-variant1">
<div class="pageWrapper">
	<!--Search Form Drawer-->
    <!--End Search Form Drawer-->
     <!-- header -->
     <?php include "header.php" ?>
    <!-- header -->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Shopping Cart</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<!-- <div class="alert alert-success text-uppercase" role="alert">
						<i class="icon anm anm-truck-l icon-large"></i> &nbsp;<strong>Congratulations!</strong> You've got free shipping!
					</div> -->
                	<form action="#" method="post" class="cart style2">
                		<table>
                            <thead class="cart__row cart__header">
                                <tr>
                                    <th  class="text-center">No</th>
                                    <th colspan="2" class="text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-right">Total</th>
                                    <th class="action">&nbsp;</th>
                                </tr>
                            </thead>
                    		<tbody id="data_keranjang">
                               
                                <?php 
                                if(!empty($_SESSION['keranjang'])){
                                    $i=1; foreach($_SESSION['keranjang'] as $id_produk => $jumlah){ 
                                   
                                    $getDataProduk = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id_produk'");
                                    $rows = $getDataProduk->fetch_assoc();
                                ?>

                                <tr class="cart__row border-bottom line1 cart-flex border-top">
                                    <td class="text-center small--hide">
                                        <span class=""><?= $i ?></span>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <a href="#"><img class="cart__image" src="admin/images/<?= $rows['foto'] ?>" width="150px" height="130px" ></a>
                                    </td>
                                    <td class="cart__meta small--text-left cart-flex-item">
                                        <div class="list-view-item__title">
                                            <a href="#"><?= $rows['nama_produk'] ?></a>
                                        </div>
                                        
                                        <div class="cart__meta-text">
                                            Color: Navy<br>Size: Small<br>
                                        </div>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item">
                                        <span class="money"><?=	"Rp. ".number_format($rows['harga_produk'],0,',','.').",-"; ?></span>
                                    </td>
                                    <td class="cart__update-wrapper cart-flex-item text-right">
                                        <div class="cart__qty text-center">
                                            <div class="qtyField">
                                                <a class="qtyBtn minus" href="javascript:void(0);" onclick="kurang_keranjang()"> <i class="icon icon-minus"></i></a>
                                                    <input class="cart__qty-input qty" name="jumlah_produk" type="text"  id="qtykeranjang<?= $i ?>" value="<?= $jumlah ?>" pattern="[0-9]*">
                                                <a class="qtyBtn plus" href="javascript:void(0);"  ><i class="icon icon-plus"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <?php $harga_produk = $rows['harga_produk'] * $jumlah;  ?>
                                        <div><span class="money"><?=	"Rp. ".number_format($harga_produk,0,',','.').",-"; ?></span></div>
                                    </td>
                                    <td class="text-center small--hide"><a href="hapus_keranjang.php?id=<?= $rows['id_produk'] ?>" class="btn btn--secondary cart__remove" title="Remove tem"><i class="icon icon anm anm-times-l"></i></a></td>
                                </tr>
                                <?php $i++; }
                                 }else{ ?>
                                     <tr class="cart__row border-bottom line1 cart-flex border-top"><td colspan="7" class="text-center small--hide mt-3 mb-3"><h2 class="mt-3 mb-3" >Keranjang Kosong</h2></td></tr>
                                 <?php } ?>
                            </tbody>
                    		<tfoot>
                                <tr>
                                  
                                    <td colspan="3" class="text-left"><a href="index.php" class="btn btn-secondary btn--small cart-continue">Continue shopping</a></td>
                                    <td colspan="4" class="text-right">
                                        <?php if(!empty($_SESSION['keranjang'])){ ?>
	                                    <a href="checkout.php" class="btn btn-secondary btn--small  small--hide">Checkout</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tfoot>
                    </table> 
                    </form>                   
               	</div>
                
                
            <div style="height: 350px;"></div>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php include "footer.php" ?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>
<script>
        $(document).ready(function() {
            $("#data_keranjang .plus").click(()=>{
                var data = $("#qty").val();
                console.log(data);
            });
        });
     </script>

<!-- belle/cart-variant1.html   11 Nov 2019 12:44:32 GMT -->
</html>