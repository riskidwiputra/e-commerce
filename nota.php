<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
    $id_pembelian   = $_GET['id'];
    $ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pelanggan.id_pelanggan=pelanggan.id_pelanggan WHERE pembelian.id_pembelian='$id_pembelian'");
    $data = $ambil->fetch_assoc();

?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/cart-variant1.html   11 Nov 2019 12:44:31 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Cart Page &ndash; Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>
<body class="page-template belle cart-variant1">
<div class="pageWrapper">
	<!--Search Form Drawer-->
    <!--End Search Form Drawer-->
    <!-- header -->
     <?php include "header.php" ?>
    <!-- header -->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Nota Pembelian</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        <div class="container mb-4" >
            <div class="row">
                <div class="col-md-3">
                    <div class="alert alert-success">
                        <div class="h3" style="font-weight: bold;">RDP Stores</div>
                        <font style="font-size: 15px">
                        <p style="margin-top: 15px; ">
                        <i class="fas fa-map-marker-alt"></i>
                        Jl. Karya Kasih Gg.kasih dalam no.37, Medan.<br> 
                        <i class="fas fa-address-book"></i>
                        +62 323 321 231 <br>
                        <i class="fas fa-envelope"></i>
                        RDPStores@example.com
                        </p>
                    </font>
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="alert alert-success" style="padding-bottom: 23px;">
                        <div class="h3" style="font-weight: bold;">Pembelian</div>
                        <font style="font-size: 16px">
                        <font style="" >Id pembelian : #<?= $data['id_pembelian']; ?></font>
                       
                            <p >
                            <i class="fas fa-calendar-alt"> <?= $data['tanggal_pembelian']; ?> </i><br>
                            
                            Status : <font style="color: red;"> Dipesan</font>
                            </p>
                        </font>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="alert alert-success" style="padding-bottom: 20px; ">
                        <div class="h3" style="font-weight: bold;">Pelanggan</div>
                        <font style="font-size: 16px">
                        <!-- <p style="margin-top: 25px; "> -->
                        <i class="fas fa-user"></i>
                    <?= $data['nama_pelanggan']; ?><br> 
                        <i class="fas fa-phone-square-alt"></i></i>
                        <?= $data['telepon']?> <br>
                    <i class="fas fa-address-book"></i>
                        <?= $data['email_pelanggan'] ?>
                        </p>
                    </font>
                   
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="alert alert-success" style="padding-bottom: 10px;">
                        <div class="h3" style="font-weight: bold;">Pengiriman</div>
                        <font style="font-size: 15px;" >
                        <p style="margin-top: 10px; ">
                       
                        <?= $data['nama_kota'];?><br> 
                        Ongkos Ongkir : <?=	"Rp. ".number_format($data['tarif'],0,',','.').",-"; ?>
                        Alamat  : <?= $data['alamat_pengiriman'] ?>
                    </font>
                  
                    </div>
                </div>
                <div class="col-12">
                    <div class="table-responsive">
                        
                         <h2>Detail Produk</h2>
                        <table class="table table-striped">
                            <thead>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Gambar Produk</th>
                                <th>Berat</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Sub Berat</th>
                                <th>Sub Total</th>
                            </thead>
                            <tbody>
                            <?php 
                            $no=1;
                                $getProduk = $koneksi->query("SELECT * FROM pembelian_produk LEFT JOIN produk ON produk.id_produk=pembelian_produk.id_produk WHERE id_pembelian='$id_pembelian' ");
                                while ($rows = $getProduk->fetch_assoc()) {
                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $rows['nama'] ?></td>
                                    <td><img src="admin/images/<?= $rows['foto'] ?>" alt=""></td>
                                    <td><?= $rows['berat'] ?> Gr.</td>
                                    <td><?=	"Rp. ".number_format($rows['harga'],0,',','.').",-"; ?></td>
                                    <td><?= $rows['jumlah'] ?></td>
                                    <td><?= $rows['sub_berat'] ?></td>
                                    <td><?=	"Rp. ".number_format($rows['sub_harga'],0,',','.').",-"; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p style="font-size: 17px; font-family: arial, sent-serif;">
                            SIlakan melakukan Pembayaran <font style="color: red; font-weight: bold;"> <?=	"Rp. ".number_format($data['total_pembelian'],0,',','.').",-"; ?></font> ke <br><font style="font-weight: bold;">  <strong>Bank Mandiri <br></strong></font>Rek : 173-000321412414 <br> AN : Blalbala  <br>
                            <br>
                            Dan Silakan konfirmasi pembayaran setelah anda membayar tagihan ini <br>
                            Produk dikirim setelah melakukan pembayaran <br>
                            Silakan Hubungi<br>
                            No : <font style="color: red; font-weight: bold;">0813-1242-6234</font> <br>
                            Wa : <font style="color: red; font-weight: bold;">0852-3215-2314</font>


                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php include "footer.php" ?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>


</html>