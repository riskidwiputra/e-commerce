 <!--Footer-->
    <footer id="footer">
      <div class="newsletter-section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-7 w-100 d-flex justify-content-start align-items-center">
              <div class="display-table">
                <div class="display-table-cell footer-newsletter">
                  <div class="section-header text-center">
                      <label class="h2"><span>sign up for </span>newsletter</label>
                  </div>
                  <form action="#" method="post">
                    <div class="input-group">
                      <input type="email" class="input-group__field newsletter__input" name="EMAIL" value="" placeholder="Email address" required="">
                      <span class="input-group__btn">
                          <button type="submit" class="btn newsletter__submit" name="commit" id="Subscribe"><span class="newsletter__submit-text--large">Subscribe</span></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-5 d-flex justify-content-end align-items-center">
              <div class="footer-social">
                <ul class="list--inline site-footer__social-icons social-icons">
                  <li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Facebook"><i class="icon icon-facebook"></i></a></li>
                  <li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Twitter"><i class="icon icon-twitter"></i> <span class="icon__fallback-text">Twitter</span></a></li>
                  <li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Instagram"><i class="icon icon-instagram"></i> <span class="icon__fallback-text">Instagram</span></a></li>
                  <li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on YouTube"><i class="icon icon-youtube"></i> <span class="icon__fallback-text">YouTube</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>    
      </div>
      <div class="site-footer">
        <div class="container-fluid">
          <!--Footer Links-->
          <div class="footer-top">
              <div class="row">
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                  <h4 class="h4">Quick Shop</h4>
                  <ul style="">
                    <?php $getkategori = $koneksi->query("SELECT * FROM kategori");
                                  while($rowkategori = $getkategori->fetch_assoc()){
                            ?>
                    <li><a href="shop.php?ms_kategori=<?= $rowkategori['id_kategori']  ?>"><?= $rowkategori['nama_kategori'] ?></a></li>

                  <?php } ?>
                  </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                  <h4 class="h4">Informations</h4>
                  <ul style="">
                    <li><a href="about-us.php">About us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Privacy policy</a></li>
                    <li><a href="#">Terms &amp; condition</a></li>
                    <li><a href="#">My Account</a></li>
                  </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                  <h4 class="h4">Customer Services</h4>
                  <ul style="">
                    <li><a href="#">Request Personal Data</a></li>
                    <li><a href="#">FAQ's</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Orders and Returns</a></li>
                    <li><a href="#">Support Center</a></li>
                  </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 contact-box">
                  <h4 class="h4">Contact Us</h4>
                  <ul class="addressFooter">
                    <li><i class="icon anm anm-map-marker-al"></i><p>55 Gallaxy Enque,<br>Jl. Karya Jaya No. 12 A, 20144 MDN</p></li>
                      <li class="phone"><i class="icon anm anm-phone-s"></i><p>(+62) 822 7340 1845</p></li>
                      <li class="email"><i class="icon anm anm-envelope-l"></i><p>achats@gmail.com</p></li>
                  </ul>
                </div>
              </div>
            </div>
              <!--End Footer Links-->
              <hr>
              <div class="footer-bottom">
                <div class="row">
                  <!--Footer Copyright-->
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12 copyright text-center"><span></span>Bengkel Pemogramman Kelompok 11</div>
                    <!--End Footer Copyright-->
                </div>
              </div>
      </div>
        </div>
    </footer>
    <!--End Footer-->