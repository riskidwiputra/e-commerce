<?php
  session_start();
  include 'koneksi.php';
?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/register.html   11 Nov 2019 12:22:27 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Create an Account &ndash; Achats Indonesia</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/achats-57x57.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle">
<div class="pageWrapper">
	<!--Search Form Drawer-->
    <!--End Search Form Drawer-->
     <!-- header -->
     <?php include "header.php" ?>
    <!-- header -->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Create an Account</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 main-col offset-md-3">
                	<div class="mb-4">
                       <form method="post" action="#" id="CustomerLoginForm" accept-charset="UTF-8" class="contact-form">	
                          <div class="row">
	                          
                               
                               <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="LastName">Username</label>
                                    <input type="text" name="username" placeholder="" id="LastName" required>
                                </div>
                               </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerEmail">Email</label>
                                    <input type="email" name="email" placeholder="" id="CustomerEmail" class="" autocorrect="off" autocapitalize="off" autofocus=""  required> 
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerPassword">Password</label>
                                    <input type="password" value="" name="password" placeholder="" id="CustomerPassword" class=""  required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="FirstName">No handphone</label>
                                    <input type="number" name="nomor" placeholder="" id="FirstName" autofocus=""  required>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                <label for="FirstName">Alamat</label>
                                    <div class="cart-note">
                                        <div class="solid-border">
                                            
                                            <textarea name="alamat" id="CartSpecialInstructions" class="cart-note__input" required> </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="text-center col-12 col-sm-12 col-md-12 col-lg-12">
                                <button class="btn mb-3" name="register">CREATE AN ACCOUNT </button>
                            </div>
                         </div>
                     </form>
                     <?php
                        if(isset($_POST['register'])){
                            $nama_pelanggan = $_POST['username'];
                            $email          = $_POST['email'];
                            $password       = $_POST['password'];
                            $nomor          = $_POST['nomor'];
                            $alamat         = $_POST['alamat'];
                            $date           = date("Y-m-d H:is");
                            $getPelanggan   = $koneksi->query("SELECT * FROM pelanggan WHERE email_pelanggan = '$email'");
                            $getcocok       = $getPelanggan->num_rows;
                            if($getcocok == 1){
                                echo "<script>alert('Pendaftaran gagal, email sudah digunakan')</script>";
                                echo "<script>location='register.php'</script>";
                            }else{
                               $koneksi->query("INSERT INTO pelanggan (email_pelanggan,password_pelanggan,nama_pelanggan,telepon,alamat,created_at) VALUE('$email','$password','$nama_pelanggan','$nomor','$alamat','$date')");
                               echo "<script>alert('Pendaftaran Sukse')</script>";
                               echo "<script>location='login.php'</script>";

                            }
                        }
                     ?>
                    </div>
               	</div>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php include "footer.php" ?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>

<!-- belle/register.html   11 Nov 2019 12:22:27 GMT -->
</html>